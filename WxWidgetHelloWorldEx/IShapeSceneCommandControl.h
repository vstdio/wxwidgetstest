#pragma once
#include "ForwardDeclarations.h"
#include "ShapeNode.h"
#include <optional>
#include <memory>

using ShapeCommandPtr = std::unique_ptr<IShapeCommand>;

class IShapeSceneCommandControl
{
public:
	virtual ~IShapeSceneCommandControl() = default;

	virtual void InsertNode(ShapeNodePtr node) = 0;
	virtual void InsertNodeAt(ShapeNodePtr node, size_t pos) = 0;

	virtual std::optional<std::pair<size_t, ShapeNodePtr>> DeletePickedNode() = 0;
	virtual ShapeNodePtr DeleteNode(size_t index) = 0;
	virtual void DeleteLastInsertedNode() = 0;

	virtual void PickNode(size_t index) = 0;
	virtual void UnpickNode() = 0;
	virtual bool IsNodePicked()const = 0;
	virtual int GetPickedNodeIndex()const = 0;

	virtual ShapeNode* GetShape(size_t index) = 0;
	virtual const ShapeNode* GetShape(size_t index)const = 0;

	virtual glm::vec2 GetSize()const = 0;
	virtual size_t GetShapesCount()const = 0;
};
