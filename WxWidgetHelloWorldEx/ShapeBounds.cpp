#include "stdafx.h"
#include "ShapeBounds.h"
#include "Math.h"
#include "ShapeScene.h"
#include "ShapeEditorView.h"
#include "Constants.h"

namespace
{

const glm::vec3 ANCHOR_FILL_COLOR = { 0, 50, 130 };
const glm::vec3 ANCHOR_STROKE_COLOR = { 0, 0, 0 };
const glm::vec3 BORDER_STROKE_COLOR = { 150, 150, 150 };
const float BORDER_STROKE_WIDTH = 2.f;

}

ShapeBounds::ShapeBounds(ShapeScene& scene)
	: m_scene(scene)
	, m_shape(nullptr)
	, m_hitBound(BoundsType::None)
	, m_stretchActions()
{
	m_stretchActions = {
		{ BoundsType::Left, std::bind(&ShapeBounds::StretchFromLeftTo, this, std::placeholders::_1) },
		{ BoundsType::Right, std::bind(&ShapeBounds::StretchFromRightTo, this, std::placeholders::_1) },
		{ BoundsType::Up, std::bind(&ShapeBounds::StretchFromUpTo, this, std::placeholders::_1) },
		{ BoundsType::Down, std::bind(&ShapeBounds::StretchFromDownTo, this, std::placeholders::_1) },
		{ BoundsType::UpLeft, std::bind(&ShapeBounds::StretchFromLeftUpTo, this, std::placeholders::_1) },
		{ BoundsType::UpRight, std::bind(&ShapeBounds::StretchFromRightUpTo, this, std::placeholders::_1) },
		{ BoundsType::DownLeft, std::bind(&ShapeBounds::StretchFromLeftDownTo, this, std::placeholders::_1) },
		{ BoundsType::DownRight, std::bind(&ShapeBounds::StretchFromRightDownTo, this, std::placeholders::_1) }
	};
}

void ShapeBounds::SetShape(ShapeNode* shape)
{
	m_shape = shape;
}

ShapeNode* ShapeBounds::GetShape()
{
	return m_shape;
}

const ShapeNode* ShapeBounds::GetShape()const
{
	return m_shape;
}

bool ShapeBounds::IsShapeSet()const
{
	return m_shape != nullptr;
}

bool ShapeBounds::IsBoundsHit() const
{
	return m_hitBound != BoundsType::None;
}

void ShapeBounds::UnhitBounds()
{
	m_hitBound = BoundsType::None;
}

BoundsType ShapeBounds::GetHitBoundsType()const
{
	return m_hitBound;
}

void ShapeBounds::SetHitBoundsType(BoundsType type)
{
	m_hitBound = type;
}

void ShapeBounds::ResizeToPos(const glm::vec2& pos)
{
	if (m_shape == nullptr)
	{
		assert(false);
		return;
	}

	auto it = m_stretchActions.find(m_hitBound);
	assert(it != m_stretchActions.end());

	it->second(pos);
}

void ShapeBounds::Draw(IRenderer& renderer)const
{
	if (m_shape == nullptr)
	{
		assert(false);
		return;
	}

	// rendering border
	glm::vec4 boundingRect = m_shape->GetBoundingRect();
	renderer.StrokeRect(boundingRect, BORDER_STROKE_COLOR, BORDER_STROKE_WIDTH);

	// rendering 4 anchors in the corners
	renderer.StrokeEllipse(glm::vec4(boundingRect.x - 4, boundingRect.y - 4, 8, 8), ANCHOR_STROKE_COLOR);
	renderer.FillEllipse(glm::vec4(boundingRect.x - 4, boundingRect.y - 4, 8, 8), ANCHOR_FILL_COLOR);

	renderer.StrokeEllipse(glm::vec4(boundingRect.x + boundingRect.z - 4, boundingRect.y - 4, 8, 8), ANCHOR_STROKE_COLOR);
	renderer.FillEllipse(glm::vec4(boundingRect.x + boundingRect.z - 4, boundingRect.y - 4, 8, 8), ANCHOR_FILL_COLOR);

	renderer.StrokeEllipse(glm::vec4(boundingRect.x + boundingRect.z - 4, boundingRect.y + boundingRect.w - 4, 8, 8), ANCHOR_STROKE_COLOR);
	renderer.FillEllipse(glm::vec4(boundingRect.x + boundingRect.z - 4, boundingRect.y + boundingRect.w - 4, 8, 8), ANCHOR_FILL_COLOR);

	renderer.StrokeEllipse(glm::vec4(boundingRect.x - 4, boundingRect.y + boundingRect.w - 4, 8, 8), ANCHOR_STROKE_COLOR);
	renderer.FillEllipse(glm::vec4(boundingRect.x - 4, boundingRect.y + boundingRect.w - 4, 8, 8), ANCHOR_FILL_COLOR);
}

BoundsType ShapeBounds::HitTest(const glm::vec2& point)const
{
	if (m_shape == nullptr)
	{
		return BoundsType::None;
	}

	glm::vec4 boundingRect = m_shape->GetBoundingRect();
	if (Math::EllipseHitTest(point, glm::vec4(boundingRect.x - 4, boundingRect.y - 4, 8, 8)))
	{
		return BoundsType::UpLeft;
	}
	if (Math::EllipseHitTest(point, glm::vec4(boundingRect.x + boundingRect.z - 4, boundingRect.y - 4, 8, 8)))
	{
		return BoundsType::UpRight;
	}
	if (Math::EllipseHitTest(point, glm::vec4(boundingRect.x + boundingRect.z - 4, boundingRect.y + boundingRect.w - 4, 8, 8)))
	{
		return BoundsType::DownRight;
	}
	if (Math::EllipseHitTest(point, glm::vec4(boundingRect.x - 4, boundingRect.y + boundingRect.w - 4, 8, 8)))
	{
		return BoundsType::DownLeft;
	}
	if (Math::RectHitTest(point, glm::vec4(boundingRect.x, boundingRect.y, boundingRect.z, 2)))
	{
		return BoundsType::Up;
	}
	if (Math::RectHitTest(point, glm::vec4(boundingRect.x, boundingRect.y, 2, boundingRect.w)))
	{
		return BoundsType::Left;
	}
	if (Math::RectHitTest(point, glm::vec4(boundingRect.x + boundingRect.z, boundingRect.y, 2, boundingRect.w)))
	{
		return BoundsType::Right;
	}
	if (Math::RectHitTest(point, glm::vec4(boundingRect.x, boundingRect.y + boundingRect.w, boundingRect.z, 2)))
	{
		return BoundsType::Down;
	}

	return BoundsType::None;
}

bool ShapeBounds::StretchFromLeftTo(const glm::vec2& pos)
{
	assert(m_shape != nullptr);
	glm::vec4 oldRect = m_shape->GetBoundingRect();

	const float offset = oldRect.x - pos.x;
	glm::vec4 newRect = oldRect;
	if (oldRect.z + offset < 0)
	{
		newRect.x += oldRect.z;
		newRect.z = std::fabsf(offset) - oldRect.z;
		m_shape->SetBoundingRect(newRect);
		m_hitBound = BoundsType::Right;
		return true;
	}
	m_shape->StretchLeft(offset);
	return false;
}

bool ShapeBounds::StretchFromRightTo(const glm::vec2& pos)
{
	assert(m_shape != nullptr);
	glm::vec4 oldRect = m_shape->GetBoundingRect();

	const float offset = pos.x - oldRect.x - oldRect.z;
	glm::vec4 newRect = oldRect;
	if (oldRect.z + offset < 0)
	{
		newRect.x -= std::fabsf(offset) - oldRect.z;
		newRect.z = std::fabsf(offset) - oldRect.z;
		m_shape->SetBoundingRect(newRect);
		m_hitBound = BoundsType::Left;
		return true;
	}
	m_shape->StretchRight(offset);
	return false;
}

bool ShapeBounds::StretchFromUpTo(const glm::vec2& pos)
{
	assert(m_shape != nullptr);
	glm::vec4 oldRect = m_shape->GetBoundingRect();

	const float offset = oldRect.y - pos.y;
	glm::vec4 newRect = oldRect;
	if (oldRect.w + offset < 0)
	{
		newRect.y += std::fabsf(offset);
		newRect.w = std::fabsf(offset) - oldRect.w;
		m_shape->SetBoundingRect(newRect);
		m_shape->Inverse();
		m_hitBound = BoundsType::Down;
		return true;
	}
	m_shape->StretchUp(offset);
	return false;
}

bool ShapeBounds::StretchFromDownTo(const glm::vec2& pos)
{
	assert(m_shape != nullptr);
	glm::vec4 oldRect = m_shape->GetBoundingRect();

	const float offset = pos.y - oldRect.y - oldRect.w;
	glm::vec4 newRect = oldRect;
	if (oldRect.w + offset < 0)
	{
		newRect.y -= std::fabsf(offset) - oldRect.w;
		newRect.w = std::fabsf(offset) - oldRect.w;
		m_shape->SetBoundingRect(newRect);
		m_shape->Inverse();
		m_hitBound = BoundsType::Up;
		return true;
	}
	m_shape->StretchDown(offset);
	return false;
}

void ShapeBounds::StretchFromLeftUpTo(const glm::vec2& pos)
{
	bool inversedUp = StretchFromUpTo(pos);
	bool inversedLeft = StretchFromLeftTo(pos);
	if (inversedUp)
	{
		m_hitBound = BoundsType::DownLeft;
	}
	if (inversedLeft)
	{
		m_hitBound = BoundsType::UpRight;
	}
	if (inversedUp && inversedLeft)
	{
		m_hitBound = BoundsType::DownRight;
	}
}

void ShapeBounds::StretchFromLeftDownTo(const glm::vec2& pos)
{
	bool inversedDown = StretchFromDownTo(pos);
	bool inversedLeft = StretchFromLeftTo(pos);
	if (inversedDown)
	{
		m_hitBound = BoundsType::UpLeft;
	}
	if (inversedLeft)
	{
		m_hitBound = BoundsType::DownRight;
	}
	if (inversedDown && inversedLeft)
	{
		m_hitBound = BoundsType::UpRight;
	}
}

void ShapeBounds::StretchFromRightUpTo(const glm::vec2& pos)
{
	bool inversedRight = StretchFromRightTo(pos);
	bool inversedUp = StretchFromUpTo(pos);
	if (inversedRight)
	{
		m_hitBound = BoundsType::UpLeft;
	}
	if (inversedUp)
	{
		m_hitBound = BoundsType::DownRight;
	}
	if (inversedRight && inversedUp)
	{
		m_hitBound = BoundsType::DownLeft;
	}
}

void ShapeBounds::StretchFromRightDownTo(const glm::vec2& pos)
{
	bool inversedRight = StretchFromRightTo(pos);
	bool inversedDown = StretchFromDownTo(pos);
	if (inversedRight)
	{
		m_hitBound = BoundsType::DownLeft;
	}
	if (inversedDown)
	{
		m_hitBound = BoundsType::UpRight;
	}
	if (inversedRight && inversedDown)
	{
		m_hitBound = BoundsType::UpLeft;
	}
}
