#pragma once
#include "ForwardDeclarations.h"
#include <optional>

class ShapesDocument
{
public:
	ShapesDocument(ShapeScene& scene);

	void New();
	void Open(const std::string& path);
	void SaveAs(const std::string& path);
	void Save();

	void SetModified();
	bool HasModified()const;
	bool HasPath()const;

private:
	ShapeScene& m_scene;
	bool m_modified;
	std::optional<std::string> m_path;
};
