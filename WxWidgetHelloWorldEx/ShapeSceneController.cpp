#include "stdafx.h"
#include "ShapeSceneController.h"
#include "ShapeEditorView.h"
#include "ShapeScene.h"
#include "AppWindow.h"
#include "InsertShapeCommand.h"
#include "Constants.h"

ShapeSceneController::ShapeSceneController(ShapeScene& shapeScene, ShapeEditorView& shapeEditorView)
	: m_shapeScene(shapeScene)
	, m_shapeEditorView(shapeEditorView)
	, m_document(shapeScene)
{
	// ������������ ����� �� ������� ������
	m_commandSignalConnection = m_shapeScene.RegisterCommandSignalHandler(
		std::bind(&ShapeSceneController::PostCommandExecute, this, std::placeholders::_1));
	m_setCursorSignalConnection = m_shapeScene.RegisterSetCursorSignalHandler(
		std::bind(&ShapeSceneController::OnSetCursorSignal, this, std::placeholders::_1));

	// ������������ ����� �� ������� ����
	m_mouseLeftDownSignalConnection = m_shapeEditorView.RegisterMouseLeftDownSignalHandler(
		std::bind(&ShapeSceneController::OnMouseLeftDown, this, std::placeholders::_1));
	m_mouseMotionSignalConnection = m_shapeEditorView.RegisterMouseMotionSignalHandler(
		std::bind(&ShapeSceneController::OnMouseMotion, this, std::placeholders::_1));
	m_mouseLeftUpSignalConnection = m_shapeEditorView.RegisterMouseLeftUpSignalHandler(
		std::bind(&ShapeSceneController::OnMouseLeftUp, this, std::placeholders::_1));
	m_mouseLeaveSignalConnection = m_shapeEditorView.RegisterMouseLeaveSignalHandler(
		std::bind(&ShapeSceneController::OnMouseLeave, this, std::placeholders::_1));
}

void ShapeSceneController::PostCommandExecute(bool commandSaved)
{
	UpdateView();
	if (commandSaved)
	{
		m_document.SetModified();
	}
}

void ShapeSceneController::OnUndoButtonDown()
{
	m_shapeScene.OnUndo();
	UpdateView();
	m_document.SetModified();
}

void ShapeSceneController::OnRedoButtonDown()
{
	m_shapeScene.OnRedo();
	UpdateView();
	m_document.SetModified();
}

void ShapeSceneController::OnCrossButtonDown()
{
	m_shapeScene.Clear();
	UpdateView();
	m_document.SetModified();
}

void ShapeSceneController::OnMouseLeftDown(const glm::ivec2& pos)
{
	m_shapeScene.OnMouseLeftDown(pos);
}

void ShapeSceneController::OnMouseMotion(const glm::ivec2& pos)
{
	if (!m_shapeScene.IsResizing())
	{
		m_shapeEditorView.SetCursor(wxCURSOR_ARROW);
	}
	m_shapeScene.OnMouseMotion(pos);
	m_shapeEditorView.GetOwner()->SetStatusText(std::to_string(pos.x) + " " + std::to_string(pos.y));
}

void ShapeSceneController::OnMouseLeftUp(const glm::ivec2& pos)
{
	m_shapeScene.OnMouseLeftUp(pos);
}

void ShapeSceneController::OnMouseLeave(const glm::ivec2& pos)
{
	m_shapeScene.OnMouseLeave(pos);
	m_shapeEditorView.GetOwner()->SetStatusText(
		(boost::format(DEFAULT_STATUSBAR_MESSAGE) % 
			ToString(m_shapeEditorView.GetRendererType())).str());
}

void ShapeSceneController::OnAddRectangleButtonDown()
{
	m_shapeScene.OnCommand(std::make_unique<InsertShapeCommand>(ShapeNodeType::Rectangle));
}

void ShapeSceneController::OnAddEllipseButtonDown()
{
	m_shapeScene.OnCommand(std::make_unique<InsertShapeCommand>(ShapeNodeType::Ellipse));
}

void ShapeSceneController::OnAddTriangleButtonDown()
{
	m_shapeScene.OnCommand(std::make_unique<InsertShapeCommand>(ShapeNodeType::Triangle));
}

void ShapeSceneController::OnSaveAs(const std::string& path)
{
	m_document.SaveAs(path);
}

bool ShapeSceneController::HasDocumentModifed()const
{
	return m_document.HasModified();
}

bool ShapeSceneController::HasDocumentPath() const
{
	return m_document.HasPath();
}

void ShapeSceneController::OnNewDocument()
{
	m_document.New();
	UpdateView();
}

void ShapeSceneController::OnOpen(const std::string& path)
{
	m_document.Open(path);
	UpdateView();
}

void ShapeSceneController::OnSave()
{
	m_document.Save();
}

void ShapeSceneController::OnSetCursorSignal(BoundsType boundsType)
{
	switch (boundsType)
	{
	case BoundsType::Left:
	case BoundsType::Right:
		m_shapeEditorView.SetCursor(wxCURSOR_SIZEWE);
		break;
	case BoundsType::Up:
	case BoundsType::Down:
		m_shapeEditorView.SetCursor(wxCURSOR_SIZENS);
		break;
	case BoundsType::UpLeft:
	case BoundsType::DownRight:
		m_shapeEditorView.SetCursor(wxCURSOR_SIZENWSE);
		break;
	case BoundsType::UpRight:
	case BoundsType::DownLeft:
		m_shapeEditorView.SetCursor(wxCURSOR_SIZENESW);
		break;
	}
}

void ShapeSceneController::UpdateView()
{
	CommandManager& commandManager = m_shapeScene.GetCommandManager();
	wxRibbonToolBar& toolbar = m_shapeEditorView.GetOwner()->GetUndoRedoToolbar();
	commandManager.UpdateUndoRedoToolbarButtons(toolbar);
	toolbar.EnableTool(wxID_UNDO, commandManager.IsUndoAvailable());
	toolbar.EnableTool(wxID_REDO, commandManager.IsRedoAvailable());
	m_shapeEditorView.Refresh(false);
}
