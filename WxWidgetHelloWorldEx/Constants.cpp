#include "stdafx.h"
#include "Constants.h"

const int NO_PICKED_NODE_INDEX = -1;

// %s will be used with boost::format
const char* const DEFAULT_STATUSBAR_MESSAGE = "Welcome to wxWidgets! Renderer: %s.";
