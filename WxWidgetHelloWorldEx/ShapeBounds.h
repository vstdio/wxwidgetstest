#pragma once

#include "ForwardDeclarations.h"
#include "IDrawable.h"

#include <unordered_map>
#include <functional>

class ShapeBounds : public IDrawable
{
public:
	ShapeBounds(ShapeScene& scene);

	void SetShape(ShapeNode* shape);
	ShapeNode* GetShape();
	const ShapeNode* GetShape()const;

	bool IsShapeSet()const;
	bool IsBoundsHit()const;
	void UnhitBounds();

	BoundsType GetHitBoundsType()const;
	void SetHitBoundsType(BoundsType type);

	void ResizeToPos(const glm::vec2& pos);
	void Draw(/*Gdiplus::Graphics& renderer*/IRenderer& renderer)const override;

	BoundsType HitTest(const glm::vec2& point)const;

private:
	bool StretchFromLeftTo(const glm::vec2& pos);
	bool StretchFromRightTo(const glm::vec2& pos);
	bool StretchFromUpTo(const glm::vec2& pos);
	bool StretchFromDownTo(const glm::vec2& pos);
	void StretchFromLeftUpTo(const glm::vec2& pos);
	void StretchFromLeftDownTo(const glm::vec2& pos);
	void StretchFromRightUpTo(const glm::vec2& pos);
	void StretchFromRightDownTo(const glm::vec2& pos);

	ShapeScene& m_scene;
	ShapeNode* m_shape;
	BoundsType m_hitBound;

	using StretchingFunction = std::function<void(const glm::vec2&)>;
	std::unordered_map<BoundsType, StretchingFunction> m_stretchActions;
};
