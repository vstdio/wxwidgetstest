#include "stdafx.h"
#include "UnpickNodeCommand.h"
#include "ShapeScene.h"

UnpickNodeCommand::UnpickNodeCommand(size_t pickedNodeIndex)
	: m_nodeIndexToUnpick(pickedNodeIndex)
{
}

void UnpickNodeCommand::Undo(IShapeSceneCommandControl& scene)
{
	scene.PickNode(m_nodeIndexToUnpick);
}

void UnpickNodeCommand::Redo(IShapeSceneCommandControl& scene)
{
	scene.UnpickNode();
}

bool UnpickNodeCommand::Execute(IShapeSceneCommandControl& scene)
{
	if (!scene.IsNodePicked())
	{
		return false;
	}
	scene.UnpickNode();
	return true;
}

std::string UnpickNodeCommand::GetName()const
{
	return "Unpick node";
}

bool UnpickNodeCommand::Merge(IShapeCommand& other)
{
	(void)other;
	return false;
}
