#include "stdafx.h"
#include "AppWindow.h"
#include "AppWindowController.h"

namespace
{
const wxPoint WINDOW_POS = { 200, 50 };
const wxSize WINDOW_SIZE = { 1024, 768 };
const std::string WINDOW_TITLE = "Shapes";
}

class MyApp : public wxApp
{
public:
	MyApp()
		: m_window(nullptr)
		, m_appWindowController(nullptr)
		, m_gdiplusStartupInput()
		, m_gdiplusToken()
	{
	}

	bool OnInit()override
	{
		Gdiplus::GdiplusStartup(&m_gdiplusToken, &m_gdiplusStartupInput, nullptr);

		m_window = new AppWindow(WINDOW_TITLE, WINDOW_POS, WINDOW_SIZE);
		if (m_window == nullptr)
		{
			return false;
		}

		m_window->Show(true);
		m_appWindowController = std::make_unique<AppWindowController>(*m_window);

		return true;
	}

	int OnExit()override
	{
		Gdiplus::GdiplusShutdown(m_gdiplusToken);
		return 0;
	}

private:
	AppWindow* m_window;
	std::unique_ptr<AppWindowController> m_appWindowController;
	Gdiplus::GdiplusStartupInput m_gdiplusStartupInput;
	ULONG_PTR m_gdiplusToken;
};

#ifdef _DEBUG
	wxIMPLEMENT_APP_CONSOLE(MyApp);
#else
	wxIMPLEMENT_APP(MyApp);
#endif
