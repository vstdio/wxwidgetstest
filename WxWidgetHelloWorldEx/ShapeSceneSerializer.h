#pragma once
#include "ForwardDeclarations.h"
#include <iosfwd>
#include <memory>
#include <string>

class ShapeSceneSerializer
{
public:
	ShapeSceneSerializer(const std::string& filePath);
	~ShapeSceneSerializer();

	void SerializeNode(const ShapeNode& shape);
	bool Flush();

private:
	class Impl;
	std::unique_ptr<Impl> m_pImpl;
};
