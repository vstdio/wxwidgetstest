// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>

// TODO: reference additional headers your program requires here

// #define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
#include <winsock2.h>
#include <windows.h>
#include "MinMaxFix.h"

#pragma warning(push, 3)
#include <gdiplus.h>
#pragma warning(pop)

#include <d2d1.h>
#include <d2d1helper.h>

#include <wx/wxprec.h>
#ifndef WX_PRECOMP
	#include <wx/wx.h>
#endif

#include <wx/graphics.h>
#include <wx/dcgraph.h>

#include <wx/ribbon/buttonbar.h>
#include <wx/ribbon/bar.h>
#include <wx/artprov.h>
#include <wx/ribbon/toolbar.h>
#include <wx/config.h>

#include <glm/geometric.hpp>
#include <glm/vec2.hpp>

#include <boost/format.hpp>

#include <algorithm>
#include <cassert>

#ifdef _DEBUG
	#include <iostream>
#endif
