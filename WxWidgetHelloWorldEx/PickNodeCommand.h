#pragma once
#include "ForwardDeclarations.h"
#include "IShapeCommand.h"
#include <optional>

class PickNodeCommand : public IShapeCommand
{
public:
	PickNodeCommand(size_t index);

	void Undo(IShapeSceneCommandControl& scene)override;
	void Redo(IShapeSceneCommandControl& scene)override;
	bool Execute(IShapeSceneCommandControl& scene)override;
	std::string GetName()const override;
	bool Merge(IShapeCommand& other)override;

private:
	size_t m_nodeIndexToPick;
	std::optional<size_t> m_alreadyPickedNode;
};
