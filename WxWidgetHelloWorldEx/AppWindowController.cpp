#include "stdafx.h"
#include "AppWindowController.h"
#include "ShapeScene.h"
#include "ShapeSceneController.h"
#include "ShapeEditorView.h"
#include "AppWindow.h"
#include "DeleteShapeCommand.h"

AppWindowController::AppWindowController(AppWindow& appWindow)
	: m_appWindow(appWindow)
	, m_shapeScene(nullptr)
	, m_sceneController(nullptr)
{
	ShapeEditorView& shapeEditorView = m_appWindow.GetShapeEditorView();
	const glm::vec2 size = { shapeEditorView.GetSize().GetWidth(), shapeEditorView.GetSize().GetHeight() };
	m_shapeScene = std::make_shared<ShapeScene>(size);
	m_sceneController = std::make_unique<ShapeSceneController>(*m_shapeScene, shapeEditorView);
	shapeEditorView.AddDrawable(m_shapeScene);

	// ���������� ����� (� ������ ������ ������) � ��������������� ��������
	// � ��������� ���������� � �������� ������� ����� �������
	m_newDocumentButtonDownSignalConnection =
		m_appWindow.RegisterNewDocumentButtonDownHandler([this]() {
			m_sceneController->OnNewDocument();
		});
	m_openDocumentButtonDownSignalConnection =
		m_appWindow.RegisterOpenDocumentButtonDownHandler([this](const std::string& path) {
			m_sceneController->OnOpen(path);
		});
	m_saveDocumentButtonDownSignalConnection =
		m_appWindow.RegisterSaveDocumentButtonDownHandler([this]() {
			m_sceneController->OnSave();
		});
	m_saveAsDocumentButtonDownSignalConnection =
		m_appWindow.RegisterSaveAsDocumentButtonDownHandler([this](const std::string& path) {
			m_sceneController->OnSaveAs(path);
		});
	m_undoButtonDownSignalConnection =
		m_appWindow.RegisterUndoButtonDownHandler([this]() {
			m_sceneController->OnUndoButtonDown();
		});
	m_redoButtonDownSignalConnection =
		m_appWindow.RegisterRedoButtonDownHandler([this]() {
			m_sceneController->OnRedoButtonDown();
		});
	m_ellipseButtonDownSignalConnection =
		m_appWindow.RegisterEllipseButtonDownHandler([this]() {
			m_sceneController->OnAddEllipseButtonDown();
		});
	m_rectangleButtonDownSignalConnection =
		m_appWindow.RegisterRectangleButtonDownHandler([this]() {
			m_sceneController->OnAddRectangleButtonDown();
		});
	m_triangleButtonDownSignalConnection =
		m_appWindow.RegisterTriangleButtonDownHandler([this]() {
			m_sceneController->OnAddTriangleButtonDown();
		});
	m_crossButtonDownSignalConnection =
		m_appWindow.RegisterCrossButtonDownHandler([this]() {
			m_sceneController->OnCrossButtonDown();
		});
	m_hasDocumentModifiedSignalConnection =
		m_appWindow.RegisterHasDocumentModifiedHandler([this]() {
			return m_sceneController->HasDocumentModifed();
		});
	m_hasDocumentPathSignalConnection =
		m_appWindow.RegisterHasDocumentPathHandler([this]() {
			return m_sceneController->HasDocumentPath();
		});
	m_keyPressedSignalConnection =
		m_appWindow.RegisterKeyPressedSignalHandler([this](int keyCode) {
			if (keyCode == WXK_DELETE)
			{
				m_shapeScene->OnCommand(std::make_unique<DeleteShapeCommand>());
			}
		});
}

AppWindowController::~AppWindowController()
{
}
