#include "stdafx.h"
#include "RectangleNode.h"
#include "Math.h"

namespace
{

const glm::vec3 RECTANGLE_DEFAULT_STROKE_COLOR = { 0, 0, 0 };
const glm::vec3 RECTANGLE_DEFAULT_FILL_COLOR = { 20, 100, 20 };

}

RectangleNode::RectangleNode(const glm::vec4& boundingRect, bool inversed)
	: ShapeNode(boundingRect, inversed)
{
}

void RectangleNode::Draw(IRenderer& renderer)const
{
	renderer.StrokeRect(m_boundingRect, RECTANGLE_DEFAULT_STROKE_COLOR);
	renderer.FillRect(m_boundingRect, RECTANGLE_DEFAULT_FILL_COLOR);
}

bool RectangleNode::HitTest(const glm::vec2& point)const
{
	return Math::RectHitTest(point, m_boundingRect);
}

ShapeNodeType RectangleNode::GetType()const
{
	return ShapeNodeType::Rectangle;
}
