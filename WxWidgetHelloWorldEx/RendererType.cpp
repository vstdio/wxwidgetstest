#include "stdafx.h"
#include "RendererType.h"

namespace
{

const std::unordered_map<RendererType, std::string> RENDERER_TYPE_MAPPING = {
	{ RendererType::Gdiplus, "GDI+" },
	{ RendererType::Direct2D, "Direct2D" }
};

}

std::string ToString(RendererType type)
{
	return RENDERER_TYPE_MAPPING.at(type);
}
