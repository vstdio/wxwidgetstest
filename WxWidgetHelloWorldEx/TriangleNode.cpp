#include "stdafx.h"
#include "TriangleNode.h"
#include "Math.h"

namespace
{

const glm::vec3 TRIANGLE_DEFAULT_STROKE_COLOR = { 0, 0, 0 };
const glm::vec3 TRIANGLE_DEFAULT_FILL_COLOR = { 200, 0, 100 };

}

TriangleNode::TriangleNode(const glm::vec4& boundingRect, bool inversed)
	: ShapeNode(boundingRect, inversed)
{
}

void TriangleNode::Draw(IRenderer& renderer)const
{
	auto points = Math::GetTrianglePoints(m_boundingRect, m_isInversed);
	renderer.StrokePolygon(points, TRIANGLE_DEFAULT_STROKE_COLOR);
	renderer.FillPolygon(points, TRIANGLE_DEFAULT_FILL_COLOR);
}

bool TriangleNode::HitTest(const glm::vec2& point)const
{
	return Math::TriangleHitTest(point, m_boundingRect, IsInversed());
}

ShapeNodeType TriangleNode::GetType()const
{
	return ShapeNodeType::Triangle;
}
