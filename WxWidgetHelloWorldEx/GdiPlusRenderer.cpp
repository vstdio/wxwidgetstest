#include "stdafx.h"
#include "GdiplusRenderer.h"

namespace
{

std::vector<Gdiplus::PointF> ToGdiplusPoints(const std::vector<glm::vec2>& glmPoints)
{
	std::vector<Gdiplus::PointF> gdiPoints(glmPoints.size());
	std::transform(glmPoints.begin(), glmPoints.end(), gdiPoints.begin(), [](auto&& point) {
		return Gdiplus::PointF{ point.x, point.y };
	});
	return gdiPoints;
}

}

class GdiplusRenderer::Impl
{
public:
	Impl(WXHDC wxHandleContextDevice)
		: m_graphics(wxHandleContextDevice)
	{
		// m_graphics.SetSmoothingMode(Gdiplus::SmoothingModeAntiAlias);
	}

	void StrokeRect(const glm::vec4& rect, const glm::vec3& color, float width)
	{
		Gdiplus::Pen pen(Gdiplus::Color(color.x, color.y, color.z), width);
		m_graphics.DrawRectangle(&pen, Gdiplus::Rect(rect.x, rect.y, rect.z, rect.w));
	}

	void FillRect(const glm::vec4& rect, const glm::vec3& color)
	{
		Gdiplus::SolidBrush brush(Gdiplus::Color(color.x, color.y, color.z));
		m_graphics.FillRectangle(&brush, Gdiplus::Rect(rect.x, rect.y, rect.z, rect.w));
	}

	void StrokeEllipse(const glm::vec4& rect, const glm::vec3& color, float width)
	{
		Gdiplus::Pen pen(Gdiplus::Color(color.x, color.y, color.z), width);
		m_graphics.DrawEllipse(&pen, Gdiplus::Rect(rect.x, rect.y, rect.z, rect.w));
	}

	void FillEllipse(const glm::vec4& rect, const glm::vec3& color)
	{
		Gdiplus::SolidBrush brush(Gdiplus::Color(color.x, color.y, color.z));
		m_graphics.FillEllipse(&brush, Gdiplus::Rect(rect.x, rect.y, rect.z, rect.w));
	}

	void StrokePolygon(const std::vector<glm::vec2>& points, const glm::vec3& color, float width)
	{
		auto gdiPoints = ToGdiplusPoints(points);
		Gdiplus::Pen pen(Gdiplus::Color(color.x, color.y, color.z), width);
		m_graphics.DrawPolygon(&pen, gdiPoints.data(), gdiPoints.size());
	}

	void FillPolygon(const std::vector<glm::vec2>& points, const glm::vec3& color)
	{
		auto gdiPoints = ToGdiplusPoints(points);
		Gdiplus::SolidBrush brush(Gdiplus::Color(color.x, color.y, color.z));
		m_graphics.FillPolygon(&brush, gdiPoints.data(), gdiPoints.size());
	}

	void Clear(const glm::vec3& color)
	{
		m_graphics.Clear(Gdiplus::Color(color.x, color.y, color.z));
	}

private:
	Gdiplus::Graphics m_graphics;
};

GdiplusRenderer::GdiplusRenderer(WXHDC wxHandleDeviceContext)
	: m_pImpl(std::make_unique<GdiplusRenderer::Impl>(wxHandleDeviceContext))
{
}

GdiplusRenderer::~GdiplusRenderer()
{
}

void GdiplusRenderer::StrokeRect(const glm::vec4& rect, const glm::vec3& color, float width)
{
	m_pImpl->StrokeRect(rect, color, width);
}

void GdiplusRenderer::FillRect(const glm::vec4& rect, const glm::vec3& color)
{
	m_pImpl->FillRect(rect, color);
}

void GdiplusRenderer::StrokeEllipse(const glm::vec4& rect, const glm::vec3& color, float width)
{
	m_pImpl->StrokeEllipse(rect, color, width);
}

void GdiplusRenderer::FillEllipse(const glm::vec4& rect, const glm::vec3& color)
{
	m_pImpl->FillEllipse(rect, color);
}

void GdiplusRenderer::StrokePolygon(const std::vector<glm::vec2>& points, const glm::vec3& color, float width)
{
	m_pImpl->StrokePolygon(points, color, width);
}

void GdiplusRenderer::FillPolygon(const std::vector<glm::vec2>& points, const glm::vec3& color)
{
	m_pImpl->FillPolygon(points, color);
}

void GdiplusRenderer::Clear(const glm::vec3& color)
{
	m_pImpl->Clear(color);
}
