#include "stdafx.h"
#include "Math.h"

namespace
{

// ��������� ���� ����� 'c' ������������ ������ 'ab'.
// ������ 0, ���� 'c' ����� �� ������ 'ab'
double GetPositionSignFromSide(const glm::vec2& a, const glm::vec2& b, const glm::vec2& c)
{
	return (c.x - a.x) * (b.y - a.y) - (c.y - a.y) * (b.x - a.x);
}

// ����� �� ����� 'c' � 'd' � ����� ������� �� ������ 'ab'
bool ArePointsFromOneSideAB(const glm::vec2& a, const glm::vec2& b, const glm::vec2& c, const glm::vec2& d)
{
	double sign1 = GetPositionSignFromSide(a, b, c);
	double sign2 = GetPositionSignFromSide(a, b, d);
	return sign1 * sign2 >= 0;
}

}

bool Math::EllipseHitTest(const glm::vec2& pos, const glm::vec4& boundingRect)
{
	// ��������� ������� (x - center.x)^2 / a^2 + (y - center.y)^2 / b^2 = 1
	// ����� (x, y) ��������� ������ ������� ����� ����������� �������� <= 1
	const glm::vec2 ellipseCenter(boundingRect.x + boundingRect.z / 2, boundingRect.y + boundingRect.w / 2);
	const float ellipseMaxRadius = boundingRect.z / 2;
	const float ellipseMinRadius = boundingRect.w / 2;
	return (pos.x - ellipseCenter.x) * (pos.x - ellipseCenter.x) / (ellipseMaxRadius * ellipseMaxRadius) +
		(pos.y - ellipseCenter.y) * (pos.y - ellipseCenter.y) / (ellipseMinRadius * ellipseMinRadius) <= 1;
}

bool Math::RectHitTest(const glm::vec2& pos, const glm::vec4& boundingRect)
{
	return (pos.x >= boundingRect.x) && (pos.x <= boundingRect.x + boundingRect.z) &&
		(pos.y >= boundingRect.y) && (pos.y <= boundingRect.y + boundingRect.w);
}

bool Math::TriangleHitTest(const glm::vec2& point, const glm::vec4& boundingRect, bool inversed)
{
	std::vector<glm::vec2> points = GetTrianglePoints(boundingRect, inversed);

	return
		ArePointsFromOneSideAB(points[0], points[1], points[2], point) &&
		ArePointsFromOneSideAB(points[1], points[2], points[0], point) &&
		ArePointsFromOneSideAB(points[2], points[0], points[1], point);
}

std::vector<glm::vec2> Math::GetTrianglePoints(const glm::vec4 & boundingRect, bool inversed)
{
	if (inversed)
	{
		return
		{
			{ boundingRect.x + boundingRect.z / 2, boundingRect.y + boundingRect.w },
			{ boundingRect.x, boundingRect.y },
			{ boundingRect.x + boundingRect.z, boundingRect.y }
		};
	}

	return
	{
		{ boundingRect.x + boundingRect.z / 2, boundingRect.y },
		{ boundingRect.x, boundingRect.y + boundingRect.w },
		{ boundingRect.x + boundingRect.z, boundingRect.y + boundingRect.w }
	};
}
