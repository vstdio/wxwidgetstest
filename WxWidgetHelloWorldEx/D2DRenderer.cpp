#include "stdafx.h"
#include "D2DRenderer.h"
#include <d2d1.h>

class D2DRendererDC::Impl
{
public:
	Impl()
		: m_pFactory(nullptr)
		, m_pRenderTarget(nullptr)
		, m_pBrush(nullptr)
	{
		HRESULT hResult = D2D1CreateFactory(D2D1_FACTORY_TYPE_SINGLE_THREADED, &m_pFactory);
		assert(hResult == S_OK); // TODO: throw exception may be better solution

		D2D1_RENDER_TARGET_PROPERTIES props = D2D1::RenderTargetProperties(
			D2D1_RENDER_TARGET_TYPE_DEFAULT,
			D2D1::PixelFormat(
				DXGI_FORMAT_B8G8R8A8_UNORM,
				D2D1_ALPHA_MODE_IGNORE),
			0,
			0,
			D2D1_RENDER_TARGET_USAGE_NONE,
			D2D1_FEATURE_LEVEL_DEFAULT
		);

		hResult = m_pFactory->CreateDCRenderTarget(&props, &m_pRenderTarget);
		assert(hResult == S_OK);

		hResult = m_pRenderTarget->CreateSolidColorBrush(D2D1::ColorF(1.f, 1.f, 1.f), &m_pBrush); // color doesn't matter here
		assert(hResult == S_OK);
	}

	~Impl()
	{
		if (m_pFactory)
		{
			m_pFactory->Release();
		}
		if (m_pRenderTarget)
		{
			m_pRenderTarget->Release();
		}
		if (m_pBrush)
		{
			m_pBrush->Release();
		}
	}

	void BeginDraw(const glm::ivec2& offset, wxPaintDC& dc)
	{
		wxRect wxClientRect;
		dc.GetClippingBox(wxClientRect);

		RECT winClientRect = { wxClientRect.GetLeft(), wxClientRect.GetTop(), wxClientRect.GetRight(), wxClientRect.GetBottom() };

		HRESULT hResult = m_pRenderTarget->BindDC(dc.GetHDC(), &winClientRect);
		assert(hResult == S_OK);

		m_pRenderTarget->BeginDraw();
		m_pRenderTarget->SetTransform(D2D1::Matrix3x2F::Translation(offset.x, offset.y));
	}

	void EndDraw()
	{
		m_pRenderTarget->EndDraw();
	}

	void StrokeRect(const glm::vec4& rect, const glm::vec3& color, float width = 1.f)
	{
		glm::vec3 normalizedColor = GetNormalizedColor(color);

		m_pBrush->SetColor(D2D1::ColorF(normalizedColor.x, normalizedColor.y, normalizedColor.z));
		D2D1_RECT_F d2dRect = { rect.x, rect.y, rect.x + rect.z, rect.y + rect.w };
		m_pRenderTarget->DrawRectangle(d2dRect, m_pBrush, width);
	}

	void FillRect(const glm::vec4& rect, const glm::vec3& color)
	{
		glm::vec3 normalizedColor = GetNormalizedColor(color);

		m_pBrush->SetColor(D2D1::ColorF(normalizedColor.x, normalizedColor.y, normalizedColor.z));
		D2D1_RECT_F d2dRect = { rect.x, rect.y, rect.x + rect.z, rect.y + rect.w };
		m_pRenderTarget->FillRectangle(d2dRect, m_pBrush);
	}

	void StrokeEllipse(const glm::vec4& rect, const glm::vec3& color, float width = 1.f)
	{
		glm::vec3 normalizedColor = GetNormalizedColor(color);

		m_pBrush->SetColor(D2D1::ColorF(normalizedColor.x, normalizedColor.y, normalizedColor.z));
		m_pRenderTarget->DrawEllipse(
			D2D1::Ellipse(
				D2D1::Point2F(rect.x + rect.z / 2, rect.y + rect.w / 2),
				rect.z / 2,
				rect.w / 2),
			m_pBrush,
			width
		);
	}

	void FillEllipse(const glm::vec4& rect, const glm::vec3 & color)
	{
		glm::vec3 normalizedColor = GetNormalizedColor(color);

		m_pBrush->SetColor(D2D1::ColorF(normalizedColor.x, normalizedColor.y, normalizedColor.z));
		m_pRenderTarget->FillEllipse(
			D2D1::Ellipse(
				D2D1::Point2F(rect.x + rect.z / 2, rect.y + rect.w / 2),
				rect.z / 2,
				rect.w / 2),
			m_pBrush
		);
	}

	void StrokePolygon(const std::vector<glm::vec2>& points, const glm::vec3& color, float width = 1.f)
	{
		glm::vec3 normalizedColor = GetNormalizedColor(color);
		m_pBrush->SetColor(D2D1::ColorF(normalizedColor.x, normalizedColor.y, normalizedColor.z));

		ID2D1PathGeometry* path;
		m_pFactory->CreatePathGeometry(&path);
		ID2D1GeometrySink* pSink;
		if (SUCCEEDED(path->Open(&pSink)))
		{
			pSink->BeginFigure(D2D1::Point2F(points[0].x, points[0].y), D2D1_FIGURE_BEGIN_FILLED);
			for (size_t i = 1; i < points.size(); ++i)
				pSink->AddLine(D2D1::Point2F(points[i].x, points[i].y));
			pSink->EndFigure(D2D1_FIGURE_END_CLOSED);
			pSink->Close();
		}
		m_pRenderTarget->DrawGeometry(path, m_pBrush, width);

		path->Release();
		pSink->Release();
	}

	void FillPolygon(const std::vector<glm::vec2>& points, const glm::vec3& color)
	{
		glm::vec3 normalizedColor = GetNormalizedColor(color);
		m_pBrush->SetColor(D2D1::ColorF(normalizedColor.x, normalizedColor.y, normalizedColor.z));

		ID2D1PathGeometry* path;
		m_pFactory->CreatePathGeometry(&path);
		ID2D1GeometrySink* pSink;
		if (SUCCEEDED(path->Open(&pSink)))
		{
			pSink->BeginFigure(D2D1::Point2F(points[0].x, points[0].y), D2D1_FIGURE_BEGIN_FILLED);
			for (size_t i = 1; i < points.size(); ++i)
				pSink->AddLine(D2D1::Point2F(points[i].x, points[i].y));
			pSink->EndFigure(D2D1_FIGURE_END_CLOSED);
			pSink->Close();
		}
		m_pRenderTarget->FillGeometry(path, m_pBrush);

		path->Release();
		pSink->Release();
	}

	void Clear(const glm::vec3& color)
	{
		glm::vec3 normalizedColor = GetNormalizedColor(color);
		m_pRenderTarget->Clear(D2D1::ColorF(normalizedColor.x, normalizedColor.y, normalizedColor.z));
	}

private:
	// ��������� ������� =)
	glm::vec3 GetNormalizedColor(const glm::vec3& color)
	{
		glm::vec3 normalized = color;

		if (normalized.x >= 1.f)
		{
			normalized.x = fmodf(normalized.x, 256.f) / 255;
		}
		if (normalized.y >= 1.f)
		{
			normalized.y = fmodf(normalized.y, 256.f) / 255;
		}
		if (normalized.z >= 1.f)
		{
			normalized.z = fmodf(normalized.z, 256.f) / 255;
		}

		return normalized;
	}

	ID2D1DCRenderTarget* m_pRenderTarget;
	ID2D1Factory* m_pFactory;
	ID2D1SolidColorBrush* m_pBrush;
};

D2DRendererDC::D2DRendererDC()
	: m_pImpl(std::make_unique<Impl>())
{
}

D2DRendererDC::~D2DRendererDC()
{
}

void D2DRendererDC::Clear(const glm::vec3& color)
{
	m_pImpl->Clear(color);
}

void D2DRendererDC::BeginDraw(const glm::ivec2& offset, wxPaintDC& dc)
{
	m_pImpl->BeginDraw(offset, dc);
}

void D2DRendererDC::EndDraw()
{
	m_pImpl->EndDraw();
}

void D2DRendererDC::StrokeRect(const glm::vec4& rect, const glm::vec3& color, float width)
{
	m_pImpl->StrokeRect(rect, color, width);
}

void D2DRendererDC::FillRect(const glm::vec4& rect, const glm::vec3& color)
{
	m_pImpl->FillRect(rect, color);
}

void D2DRendererDC::StrokeEllipse(const glm::vec4& rect, const glm::vec3& color, float width)
{
	m_pImpl->StrokeEllipse(rect, color, width);
}

void D2DRendererDC::FillEllipse(const glm::vec4& rect, const glm::vec3& color)
{
	m_pImpl->FillEllipse(rect, color);
}

void D2DRendererDC::StrokePolygon(const std::vector<glm::vec2>& points, const glm::vec3& color, float width)
{
	m_pImpl->StrokePolygon(points, color, width);
}

void D2DRendererDC::FillPolygon(const std::vector<glm::vec2>& points, const glm::vec3& color)
{
	m_pImpl->FillPolygon(points, color);
}
