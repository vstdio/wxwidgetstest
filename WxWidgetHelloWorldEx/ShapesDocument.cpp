#include "stdafx.h"
#include "ShapesDocument.h"
#include "ShapeScene.h"

ShapesDocument::ShapesDocument(ShapeScene& scene)
	: m_scene(scene)
	, m_modified(false)
	, m_path(std::nullopt)
{
}

void ShapesDocument::New()
{
	m_scene.Clear();
	m_path = std::nullopt;
	m_modified = false;
}

void ShapesDocument::Open(const std::string& path)
{
	m_scene.LoadFromFile(path);
	m_path = path;
	m_modified = false;
}

void ShapesDocument::SaveAs(const std::string& path)
{
	m_scene.SerializeToFile(path);
	m_path = path;
	m_modified = false;
}

void ShapesDocument::Save()
{
	assert(m_path);
	SaveAs(m_path.value());
}

void ShapesDocument::SetModified()
{
	m_modified = true;
}

bool ShapesDocument::HasModified()const
{
	return m_modified;
}

bool ShapesDocument::HasPath()const
{
	return m_path != std::nullopt;
}
