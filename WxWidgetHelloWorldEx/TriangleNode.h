#pragma once
#include "ShapeNode.h"
#include <vector>

class TriangleNode : public ShapeNode
{
public:
	explicit TriangleNode(const glm::vec4& boundingRect, bool inversed = false);
	void Draw(IRenderer& renderer)const override;
	bool HitTest(const glm::vec2& point)const override;
	ShapeNodeType GetType()const override;
};
