#pragma once
#include "IShapeCommand.h"
#include "ForwardDeclarations.h"
#include "ShapeNode.h"

class DeleteShapeCommand : public IShapeCommand
{
public:
	DeleteShapeCommand();

	void Undo(IShapeSceneCommandControl& scene)override;
	void Redo(IShapeSceneCommandControl& scene)override;
	bool Execute(IShapeSceneCommandControl& scene)override;
	std::string GetName()const override;
	bool Merge(IShapeCommand& other)override;

private:
	int m_deletedNodeIndex;
	ShapeNodePtr m_deletedNode;
};
