#pragma once
#include <glm/fwd.hpp>

class Math
{
public:
	Math() = delete;
	static bool EllipseHitTest(const glm::vec2& point, const glm::vec4& boundingRect);
	static bool RectHitTest(const glm::vec2& point, const glm::vec4& boundingRect);
	static bool TriangleHitTest(const glm::vec2& point, const glm::vec4& boundingRect, bool inversed = false);
	static std::vector<glm::vec2> GetTrianglePoints(const glm::vec4& boundingRect, bool inversed = false);
};
