#include "stdafx.h"
#include "MoveShapeCommand.h"
#include "ShapeNode.h"
#include "ShapeScene.h"

MoveShapeCommand::MoveShapeCommand(size_t shapeIndex, const glm::vec2& newPos)
	: m_shapeIndex(shapeIndex)
	, m_newPos(newPos)
{
}

void MoveShapeCommand::Undo(IShapeSceneCommandControl& scene)
{
	auto shape = scene.GetShape(m_shapeIndex);
	assert(shape != nullptr);
	shape->MoveTo(m_oldPos);
}

void MoveShapeCommand::Redo(IShapeSceneCommandControl& scene)
{
	Execute(scene);
}

bool MoveShapeCommand::Execute(IShapeSceneCommandControl& scene)
{
	auto shape = scene.GetShape(m_shapeIndex);
	assert(shape != nullptr);
	m_oldPos = shape->GetPosition();
	shape->MoveTo(m_newPos);
	return true;
}

std::string MoveShapeCommand::GetName() const
{
	return "Move shape";
}

bool MoveShapeCommand::Merge(IShapeCommand& other)
{
	if (other.GetName() == "Move shape")
	{
		m_newPos = reinterpret_cast<MoveShapeCommand&>(other).GetNewPos();
		return true;
	}
	return false;
}

glm::vec2 MoveShapeCommand::GetNewPos()const
{
	return m_newPos;
}

glm::vec2 MoveShapeCommand::GetOldPos()const
{
	return m_oldPos;
}
