#pragma once
#include "ShapeNode.h"

class EllipseNode : public ShapeNode
{
public:
	explicit EllipseNode(const glm::vec4& boundingRect, bool inversed = false);
	void Draw(IRenderer& renderer)const override;
	bool HitTest(const glm::vec2& point)const override;
	ShapeNodeType GetType()const override;
};
