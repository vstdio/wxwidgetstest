#include "stdafx.h"
#include "ShapeEditorView.h"
#include "AppWindow.h"
#include "GdiplusRenderer.h"
#include "D2DRenderer.h"

namespace
{

const glm::vec3 CLEAR_COLOR = { 255.f, 255.f, 255.f };

}

ShapeEditorView::ShapeEditorView(AppWindow* parent)
	: wxScrolledWindow(reinterpret_cast<wxPanel*>(parent))
	, m_owner(parent)
	, m_d2dRendererDC(nullptr)
	, m_gdiplusRenderer(nullptr) // will be initialized after PrepareDC()
	, m_scrollOffset(0, 0)
	, m_rendererType(RendererType::Gdiplus)
{
	wxScrolledWindow::Connect(wxID_ANY, wxEVT_MOTION, wxMouseEventHandler(ShapeEditorView::OnMouseMotion), nullptr, this);
	wxScrolledWindow::Connect(wxEVT_LEFT_DOWN, wxMouseEventHandler(ShapeEditorView::OnMouseLeftDown));
	wxScrolledWindow::Connect(wxEVT_PAINT, wxPaintEventHandler(ShapeEditorView::OnPaintEvent));

	m_d2dRendererDC = std::make_unique<D2DRendererDC>();
	wxScrolledWindow::SetDoubleBuffered(true);
}

AppWindow* ShapeEditorView::GetOwner()
{
	return m_owner;
}

const AppWindow* ShapeEditorView::GetOwner()const
{
	return m_owner;
}

void ShapeEditorView::AddDrawable(const std::shared_ptr<IDrawable>& drawable)
{
	m_drawables.push_back(drawable);
}

void ShapeEditorView::SetRendererType(RendererType rendererType)
{
	m_rendererType = rendererType;
}

RendererType ShapeEditorView::GetRendererType()const
{
	return m_rendererType;
}

void ShapeEditorView::Render(wxPaintDC& dc)
{
	switch (m_rendererType)
	{
	case RendererType::Gdiplus:
		m_gdiplusRenderer = std::make_unique<GdiplusRenderer>(dc.GetHDC());
		m_gdiplusRenderer->Clear(CLEAR_COLOR);
		for (const auto& drawable : m_drawables)
		{
			drawable->Draw(*m_gdiplusRenderer);
}
		break;
	case RendererType::Direct2D:
		m_d2dRendererDC->BeginDraw(-m_scrollOffset, dc); // negative scroll offset for transforming drawable objects
		m_d2dRendererDC->Clear(CLEAR_COLOR);
		for (const auto& drawable : m_drawables)
		{
			drawable->Draw(*m_d2dRendererDC);
		}
		m_d2dRendererDC->EndDraw();
		break;
	default:
		throw std::logic_error("ShapeEditorView::Render(): default branch should be unreachable");
	}
}

void ShapeEditorView::OnMouseLeftDown(wxMouseEvent& event)
{
	const glm::ivec2 screenPos = { event.GetPosition().x, event.GetPosition().y };
	const glm::ivec2 relativePos = { GetViewStart().x + screenPos.x, GetViewStart().y + screenPos.y };

	m_mouseLeftDownSignal(relativePos);
}

void ShapeEditorView::OnMouseMotion(wxMouseEvent& event)
{
	const glm::ivec2 screenPos = { event.GetPosition().x, event.GetPosition().y };
	const glm::ivec2 relativePos = { GetViewStart().x + screenPos.x, GetViewStart().y + screenPos.y };

	m_mouseMotionSignal(relativePos);
}

void ShapeEditorView::OnMouseLeftUp(wxMouseEvent& event)
{
	const glm::ivec2 screenPos = { event.GetPosition().x, event.GetPosition().y };
	const glm::ivec2 relativePos = { GetViewStart().x + screenPos.x, GetViewStart().y + screenPos.y };

	m_mouseLeftUpSignal(relativePos);
}

void ShapeEditorView::OnMouseLeave(wxMouseEvent& event)
{
	const glm::ivec2 screenPos = { event.GetPosition().x, event.GetPosition().y };
	const glm::ivec2 relativePos = { GetViewStart().x + screenPos.x, GetViewStart().y + screenPos.y };

	m_mouseLeaveSignal(relativePos);
}

void ShapeEditorView::OnScrollEvent(wxScrollWinEvent& event)
{
	if (event.GetOrientation() == wxHORIZONTAL)
	{
		m_scrollOffset.x = wxScrolledWindow::GetScrollPos(wxHORIZONTAL);
	}
	else if (event.GetOrientation() == wxVERTICAL)
	{
		m_scrollOffset.y = wxScrolledWindow::GetScrollPos(wxVERTICAL);
	}

	event.Skip(true);
}

void ShapeEditorView::OnResizeEvent(wxSizeEvent& event)
{
	m_scrollOffset.x = wxScrolledWindow::GetScrollPos(wxHORIZONTAL);
	m_scrollOffset.y = wxScrolledWindow::GetScrollPos(wxVERTICAL);
	wxScrolledWindow::Refresh(true);
	event.Skip(true);
}

void ShapeEditorView::OnPaintEvent(wxPaintEvent& WXUNUSED(event))
{
	wxPaintDC dc(this);
	PrepareDC(dc);
	Render(dc);
}

ScopedConnection ShapeEditorView::RegisterMouseLeftDownSignalHandler(MouseSignal::slot_type handler)
{
	return m_mouseLeftDownSignal.connect(handler);
}

ScopedConnection ShapeEditorView::RegisterMouseMotionSignalHandler(MouseSignal::slot_type handler)
{
	return m_mouseMotionSignal.connect(handler);
}

ScopedConnection ShapeEditorView::RegisterMouseLeftUpSignalHandler(MouseSignal::slot_type handler)
{
	return m_mouseLeftUpSignal.connect(handler);
}

ScopedConnection ShapeEditorView::RegisterMouseLeaveSignalHandler(MouseSignal::slot_type handler)
{
	return m_mouseLeaveSignal.connect(handler);
}

wxBEGIN_EVENT_TABLE(ShapeEditorView, wxPanel)
	EVT_LEFT_DOWN(ShapeEditorView::OnMouseLeftDown)
	EVT_LEFT_UP(ShapeEditorView::OnMouseLeftUp)
	EVT_MOTION(ShapeEditorView::OnMouseMotion)
	EVT_LEAVE_WINDOW(ShapeEditorView::OnMouseLeave)
	EVT_PAINT(ShapeEditorView::OnPaintEvent)
	EVT_SCROLLWIN(ShapeEditorView::OnScrollEvent)
	EVT_SIZE(ShapeEditorView::OnResizeEvent)
wxEND_EVENT_TABLE()
