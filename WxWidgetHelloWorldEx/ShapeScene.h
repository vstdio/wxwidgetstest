#pragma once

#include "ForwardDeclarations.h"
#include "ShapeNode.h"
#include "ShapeBounds.h"
#include "IShapeCommand.h"
#include "CommandManager.h"
#include "IDrawable.h"
#include "Aliases.h"

#include <memory>
#include <vector>

#include <glm/vec2.hpp>
#include <optional>

class ShapeScene
	: public IDrawable
	, private IShapeSceneCommandControl
{
public:
	ShapeScene(const glm::ivec2& size,
		std::vector<ShapeNodePtr> && shapes = std::vector<ShapeNodePtr>());

	void SerializeToFile(const std::string& path)const;
	void LoadFromFile(const std::string& path);

	void OnCommand(ShapeCommandPtr command);
	void OnUndo();
	void OnRedo();

	void Clear();
	bool IsResizing()const;

	void OnMouseLeftDown(const glm::vec2& pos);
	void OnMouseMotion(const glm::vec2& pos);
	void OnMouseLeftUp(const glm::vec2& pos);
	void OnMouseLeave(const glm::vec2& pos);

	void Draw(IRenderer& renderer)const override;

	CommandManager& GetCommandManager();
	const CommandManager& GetCommandManager()const;

	ScopedConnection RegisterCommandSignalHandler(PostCommandSignal::slot_type handler);
	ScopedConnection RegisterSetCursorSignalHandler(BoundsHoverSignal::slot_type handler);

private:
	// Derived from IShapeSceneCommandControl
	void InsertNode(ShapeNodePtr node)override;
	void InsertNodeAt(ShapeNodePtr node, size_t pos)override;

	ShapeNode* GetShape(size_t index)override;
	const ShapeNode* GetShape(size_t index)const override;

	void PickNode(size_t index)override;
	void UnpickNode()override;
	bool IsNodePicked()const override;
	int GetPickedNodeIndex()const override;

	glm::vec2 GetSize()const override;
	void SetSize(const glm::vec2& size);
	size_t GetShapesCount()const override;

	// ���������� ������ � ������ � ����� �������� ������ � ������ ������, ����� nullopt
	std::optional<std::pair<size_t, ShapeNodePtr>> DeletePickedNode()override;
	ShapeNodePtr DeleteNode(size_t index)override;
	void DeleteLastInsertedNode()override;
	//

	// ���������� ������ ������ � �������, ���� � ���������� �������� � �������� �����,
	// ����� ������������ NO_PICKED_NODE_INDEX
	int GetNodeIndexToPick(const glm::vec2& pos);

	void OnDragBegin(const glm::vec2& startPos);
	void OnDragEnd();

	PostCommandSignal m_commandSignal;
	BoundsHoverSignal m_setCursorSignal;

	glm::vec2 m_dragOffset;
	glm::ivec2 m_size;

	ShapeBounds m_shapeBounds;
	CommandManager m_commandManager;

	std::vector<ShapeNodePtr> m_shapes;

	// ��������: ���������� ����� �������� NO_PICKED_NODE_INDEX,
	// ����� �� ���� ������ �� ����� �� �������, ����� �������� � ����
	// ���������� ������ ������ � ������� m_shapes.
	// ����� ���������� �� ��������� ����� ������� ��������� �������.
	int m_pickedNodeIndex;
	bool m_isDragging;
	bool m_isResizing;
};

using ShapeScenePtr = std::unique_ptr<ShapeScene>;
