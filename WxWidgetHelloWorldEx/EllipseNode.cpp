#include "stdafx.h"
#include "EllipseNode.h"
#include "Math.h"

namespace
{

const glm::vec3 ELLIPSE_DEFAULT_STROKE_COLOR = { 0, 0, 0 };
const glm::vec3 ELLIPSE_DEFAULT_FILL_COLOR = { 200, 150, 40 };

}

EllipseNode::EllipseNode(const glm::vec4& boundingRect, bool inversed)
	: ShapeNode(boundingRect, inversed)
{
}

void EllipseNode::Draw(IRenderer& renderer)const
{
	renderer.StrokeEllipse(m_boundingRect, ELLIPSE_DEFAULT_STROKE_COLOR);
	renderer.FillEllipse(m_boundingRect, ELLIPSE_DEFAULT_FILL_COLOR);
}

bool EllipseNode::HitTest(const glm::vec2& point)const
{
	return Math::EllipseHitTest(point, m_boundingRect);
}

ShapeNodeType EllipseNode::GetType()const
{
	return ShapeNodeType::Ellipse;
}
