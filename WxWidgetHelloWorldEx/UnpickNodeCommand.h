#pragma once
#include "ForwardDeclarations.h"
#include "IShapeCommand.h"

class UnpickNodeCommand : public IShapeCommand
{
public:
	UnpickNodeCommand(size_t pickedNodeIndex);

	void Undo(IShapeSceneCommandControl& scene)override;
	void Redo(IShapeSceneCommandControl& scene)override;
	bool Execute(IShapeSceneCommandControl& scene)override;
	std::string GetName()const override;
	bool Merge(IShapeCommand& other)override;

private:
	size_t m_nodeIndexToUnpick;
};
