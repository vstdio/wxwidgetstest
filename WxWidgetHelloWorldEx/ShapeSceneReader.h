#pragma once
#include "ForwardDeclarations.h"
#include <vector>
#include <string>
#include <memory>

//? can be replaced with 1 function or static method
class ShapeSceneReader
{
public:
	ShapeSceneReader(const std::string& filePath);
	std::vector<std::unique_ptr<ShapeNode>> GetShapes();

private:
	std::string m_filePath;
};
