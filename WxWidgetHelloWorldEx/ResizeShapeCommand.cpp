#include "stdafx.h"
#include "ResizeShapeCommand.h"
#include "ShapeScene.h"
#include "ShapeBounds.h"

ResizeShapeCommand::ResizeShapeCommand(ShapeBounds& bounds, const glm::vec2& newPos)
	: m_bounds(bounds)
	, m_newPos(newPos)
	, m_stateForRedo()
	, m_stateForUndo()
{
}

void ResizeShapeCommand::Undo(IShapeSceneCommandControl& scene)
{
	(void)scene;

	assert(m_bounds.GetShape() != nullptr);
	m_stateForRedo.boundingRect = m_bounds.GetShape()->GetBoundingRect();
	m_stateForRedo.boundsType = m_bounds.GetHitBoundsType();
	m_stateForRedo.isInversed = m_bounds.GetShape()->IsInversed();

	m_bounds.SetHitBoundsType(m_stateForUndo.boundsType);
	m_bounds.GetShape()->SetBoundingRect(m_stateForUndo.boundingRect);
	m_bounds.GetShape()->SetInversed(m_stateForUndo.isInversed);

	m_bounds.UnhitBounds();
}

void ResizeShapeCommand::Redo(IShapeSceneCommandControl& scene)
{
	(void)scene;

	assert(m_bounds.GetShape() != nullptr);
	m_stateForUndo.boundingRect = m_bounds.GetShape()->GetBoundingRect();
	m_stateForUndo.boundsType = m_bounds.GetHitBoundsType();
	m_stateForUndo.isInversed = m_bounds.GetShape()->IsInversed();

	m_bounds.SetHitBoundsType(m_stateForRedo.boundsType);
	m_bounds.GetShape()->SetBoundingRect(m_stateForRedo.boundingRect);
	m_bounds.GetShape()->SetInversed(m_stateForRedo.isInversed);

	m_bounds.UnhitBounds();
}

bool ResizeShapeCommand::Execute(IShapeSceneCommandControl& scene)
{
	(void)scene;

	assert(m_bounds.GetShape() != nullptr);
	if (m_bounds.IsBoundsHit())
	{
		m_stateForUndo.boundingRect = m_bounds.GetShape()->GetBoundingRect();
		m_stateForUndo.boundsType = m_bounds.GetHitBoundsType();
		m_stateForUndo.isInversed = m_bounds.GetShape()->IsInversed();
		m_bounds.ResizeToPos(m_newPos);
		return true;
	}
	return false;
}

std::string ResizeShapeCommand::GetName()const
{
	return "Resize shape";
}

bool ResizeShapeCommand::Merge(IShapeCommand& other)
{
	return other.GetName() == "Resize shape";
}
