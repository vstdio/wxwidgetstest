#pragma once

enum class RendererType
{
	Gdiplus,
	Direct2D
};

std::string ToString(RendererType type);
