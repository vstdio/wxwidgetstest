#pragma once
#include "ForwardDeclarations.h"
#include "IRenderer.h"

class IDrawable
{
public:
	virtual ~IDrawable() = default;
	virtual void Draw(IRenderer& renderer)const = 0;
};
