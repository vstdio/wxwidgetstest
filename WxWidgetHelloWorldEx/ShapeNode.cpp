#include "stdafx.h"
#include "ShapeNode.h"
#include "Math.h"

ShapeNode::ShapeNode(const glm::vec4& boundingRect, bool inversed)
	: m_boundingRect(boundingRect)
	, m_isInversed(inversed)
{
}

glm::vec2 ShapeNode::GetPosition()const
{
	return glm::vec2(m_boundingRect.x, m_boundingRect.y);
}

glm::vec4 ShapeNode::GetBoundingRect() const
{
	return m_boundingRect;
}

void ShapeNode::SetBoundingRect(const glm::vec4& boundingRect)
{
	m_boundingRect = boundingRect;
}

bool ShapeNode::StretchUp(float offset)
{
	m_boundingRect.y -= offset;
	m_boundingRect.w += offset;
	return false;
}

bool ShapeNode::StretchDown(float offset)
{
	m_boundingRect.w += offset;
	return false;
}

bool ShapeNode::StretchLeft(float offset)
{
	m_boundingRect.x -= offset;
	m_boundingRect.z += offset;
	return false;
}

bool ShapeNode::StretchRight(float offset)
{
	m_boundingRect.z += offset;
	return false;
}

void ShapeNode::AddWidth(float width)
{
	m_boundingRect.z += width;
}

void ShapeNode::AddHeight(float height)
{
	m_boundingRect.w += height;
}

void ShapeNode::Inverse()
{
	m_isInversed = !m_isInversed;
}

void ShapeNode::SetInversed(bool inversed)
{
	m_isInversed = inversed;
}

bool ShapeNode::IsInversed()const
{
	return m_isInversed;
}

void ShapeNode::MoveBy(const glm::vec2& offset)
{
	m_boundingRect.x += offset.x;
	m_boundingRect.y += offset.y;
}

void ShapeNode::MoveTo(const glm::vec2& position)
{
	m_boundingRect.x = position.x;
	m_boundingRect.y = position.y;
}
