#pragma once
#include <glm/fwd.hpp>
#include <vector>

class IRenderer
{
public:
	virtual ~IRenderer() = default;

	virtual void StrokeRect(const glm::vec4& rect, const glm::vec3& color, float width = 1.f) = 0;
	virtual void FillRect(const glm::vec4& rect, const glm::vec3& color) = 0;

	virtual void StrokeEllipse(const glm::vec4& rect, const glm::vec3& color, float width = 1.f) = 0;
	virtual void FillEllipse(const glm::vec4& rect, const glm::vec3& color) = 0;

	virtual void StrokePolygon(const std::vector<glm::vec2>& points, const glm::vec3& color, float width = 1.f) = 0;
	virtual void FillPolygon(const std::vector<glm::vec2>& points, const glm::vec3& color) = 0;

	virtual void Clear(const glm::vec3& color) = 0;
};
