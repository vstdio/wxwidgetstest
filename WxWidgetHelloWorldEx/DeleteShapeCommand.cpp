#include "stdafx.h"
#include "ShapeScene.h"
#include "DeleteShapeCommand.h"
#include "Constants.h"

DeleteShapeCommand::DeleteShapeCommand()
	: m_deletedNode(nullptr)
	, m_deletedNodeIndex(NO_PICKED_NODE_INDEX)
{
}

void DeleteShapeCommand::Undo(IShapeSceneCommandControl& scene)
{
	assert(m_deletedNode != nullptr);
	assert(m_deletedNodeIndex != NO_PICKED_NODE_INDEX);
	scene.InsertNodeAt(std::move(m_deletedNode), m_deletedNodeIndex);
	scene.PickNode(m_deletedNodeIndex);
}

void DeleteShapeCommand::Redo(IShapeSceneCommandControl& scene)
{
	m_deletedNode = std::move(scene.DeleteNode(m_deletedNodeIndex));
}

bool DeleteShapeCommand::Execute(IShapeSceneCommandControl& scene)
{
	if (auto deletedShapePair = scene.DeletePickedNode())
	{
		m_deletedNodeIndex = deletedShapePair->first;
		m_deletedNode = std::move(deletedShapePair->second);
		return true;
	}
	return false;
}

std::string DeleteShapeCommand::GetName()const
{
	return "Delete shape";
}

bool DeleteShapeCommand::Merge(IShapeCommand& other)
{
	(void)other;
	return false;
}
