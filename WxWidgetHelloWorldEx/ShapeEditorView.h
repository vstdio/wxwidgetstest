#pragma once

#include <wx/panel.h>
#include <wx/frame.h>

#include <vector>
#include <memory>

#include "ForwardDeclarations.h"
#include "RendererType.h"
#include "ShapeScene.h"
#include "IShapeCommand.h"
#include "Aliases.h"

class ShapeEditorView : public wxScrolledWindow
{
public:
	ShapeEditorView(AppWindow* parent);

	AppWindow* GetOwner();
	const AppWindow* GetOwner()const;

	void AddDrawable(const std::shared_ptr<IDrawable>& drawable);
	void SetRendererType(RendererType rendererType);
	RendererType GetRendererType()const;

	ScopedConnection RegisterMouseLeftDownSignalHandler(MouseSignal::slot_type handler);
	ScopedConnection RegisterMouseMotionSignalHandler(MouseSignal::slot_type handler);
	ScopedConnection RegisterMouseLeftUpSignalHandler(MouseSignal::slot_type handler);
	ScopedConnection RegisterMouseLeaveSignalHandler(MouseSignal::slot_type handler);

private:
	void Render(wxPaintDC& dc);

	void OnMouseLeftDown(wxMouseEvent& event);
	void OnMouseLeftUp(wxMouseEvent& event);
	void OnMouseMotion(wxMouseEvent& event);
	void OnMouseLeave(wxMouseEvent& event);
	void OnScrollEvent(wxScrollWinEvent& event);
	void OnResizeEvent(wxSizeEvent& event);
	void OnPaintEvent(wxPaintEvent& event);
	wxDECLARE_EVENT_TABLE();

	AppWindow* m_owner;
	std::vector<std::shared_ptr<IDrawable>> m_drawables;

	std::unique_ptr<D2DRendererDC> m_d2dRendererDC;
	std::unique_ptr<GdiplusRenderer> m_gdiplusRenderer;
	RendererType m_rendererType;

	glm::ivec2 m_scrollOffset;

	MouseSignal m_mouseLeftDownSignal;
	MouseSignal m_mouseMotionSignal;
	MouseSignal m_mouseLeftUpSignal;
	MouseSignal m_mouseLeaveSignal;
};
