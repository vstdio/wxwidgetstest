#include "stdafx.h"
#include "CommandManager.h"
#include "IShapeCommand.h"

CommandManager::CommandManager(IShapeSceneCommandControl& scene)
	: m_undoStack()
	, m_redoStack()
	, m_scene(scene)
	, m_merge(true)
{
}

void CommandManager::SaveCommand(ShapeCommandPtr command)
{
	if (m_merge && MergeCommands(*command.get()))
	{
		return;
	}
	m_undoStack.push_back(std::move(command));
	m_redoStack.clear();
	m_merge = true;
}

void CommandManager::DisableMergeOnce()
{
	m_merge = false;
}

void CommandManager::Clear()
{
	m_redoStack.clear();
	m_undoStack.clear();
}

void CommandManager::Redo()
{
	if (IsRedoAvailable())
	{
		ShapeCommandPtr command = std::move(m_redoStack.back());
		m_redoStack.pop_back();
		command->Redo(m_scene);
		m_undoStack.push_back(std::move(command));
	}
}

void CommandManager::Undo()
{
	if (IsUndoAvailable())
	{
		ShapeCommandPtr command = std::move(m_undoStack.back());
		m_undoStack.pop_back();
		command->Undo(m_scene);
		m_redoStack.push_back(std::move(command));
	}
}

bool CommandManager::IsUndoAvailable()const
{
	return !m_undoStack.empty();
}

bool CommandManager::IsRedoAvailable()const
{
	return !m_redoStack.empty();
}

void CommandManager::UpdateUndoRedoToolbarButtons(wxRibbonToolBar& toolbar)const
{
	toolbar.SetToolHelpString(wxID_UNDO, IsUndoAvailable() ? "Undo: [" + m_undoStack.back()->GetName() + "]" : "Undo");
	toolbar.SetToolHelpString(wxID_REDO, IsRedoAvailable() ? "Redo: [" + m_redoStack.back()->GetName() + "]" : "Redo");
}

bool CommandManager::MergeCommands(IShapeCommand& pCommand)
{
	if (m_undoStack.empty())
	{
		return false;
	}
	return m_undoStack.back()->Merge(pCommand);
}
