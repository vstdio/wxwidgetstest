#pragma once
#include "ForwardDeclarations.h"
#include "IShapeCommand.h"

class ResizeShapeCommand : public IShapeCommand
{
	struct ShapeBoundsState
	{
		glm::vec4 boundingRect;
		BoundsType boundsType;
		bool isInversed;
	};

public:
	ResizeShapeCommand(ShapeBounds& bounds, const glm::vec2& newPos);

	void Undo(IShapeSceneCommandControl& scene)override;
	void Redo(IShapeSceneCommandControl& scene)override;
	bool Execute(IShapeSceneCommandControl& scene)override;
	std::string GetName()const override;
	bool Merge(IShapeCommand& other)override;

private:
	ShapeBounds& m_bounds;
	glm::vec2 m_newPos;
	ShapeBoundsState m_stateForUndo;
	ShapeBoundsState m_stateForRedo;
};
