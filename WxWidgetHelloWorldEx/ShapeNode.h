#pragma once
#include "ForwardDeclarations.h"
#include "IDrawable.h"
#include <glm/vec4.hpp>
#include <memory>

class ShapeNode : public IDrawable
{
public:
	explicit ShapeNode(const glm::vec4& boundingRect, bool inversed = false);

	glm::vec2 GetPosition()const;
	glm::vec4 GetBoundingRect()const;

	void SetBoundingRect(const glm::vec4& boundingRect);

	bool StretchUp(float offset);
	bool StretchDown(float offset);
	bool StretchLeft(float offset);
	bool StretchRight(float offset);

	void AddWidth(float width);
	void AddHeight(float height);

	void Inverse();
	void SetInversed(bool inversed);
	bool IsInversed()const;

	virtual void MoveBy(const glm::vec2& offset);
	virtual void MoveTo(const glm::vec2& position);

	virtual bool HitTest(const glm::vec2& point)const = 0;
	virtual void Draw(IRenderer& renderer)const = 0;
	virtual ShapeNodeType GetType()const = 0;

protected:
	glm::vec4 m_boundingRect;
	bool m_isInversed;
};

using ShapeNodePtr = std::unique_ptr<ShapeNode>;
