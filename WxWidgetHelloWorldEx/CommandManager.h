#pragma once
#include "ForwardDeclarations.h"
#include "IShapeSceneCommandControl.h"
#include <vector>

class CommandManager
{
public:
	CommandManager(IShapeSceneCommandControl& scene);

	void SaveCommand(ShapeCommandPtr command);
	void DisableMergeOnce();

	void Clear();

	void Redo();
	void Undo();

	bool IsUndoAvailable()const;
	bool IsRedoAvailable()const;

	void UpdateUndoRedoToolbarButtons(wxRibbonToolBar& toolbar)const;

private:
	bool MergeCommands(IShapeCommand& pCommand);

private:
	std::vector<ShapeCommandPtr> m_undoStack;
	std::vector<ShapeCommandPtr> m_redoStack;
	IShapeSceneCommandControl& m_scene;
	bool m_merge;
};
