#pragma once
#include <wx/frame.h>
#include "ShapeEditorView.h"
#include "Aliases.h"

class AppWindow : public wxFrame
{
public:
	AppWindow(const wxString& title, const wxPoint& position, const wxSize& size);

	wxRibbonToolBar& GetUndoRedoToolbar();
	const wxRibbonToolBar& GetUndoRedoToolbar()const;

	ShapeEditorView& GetShapeEditorView();
	const ShapeEditorView& GetShapeEditorView()const;

	ScopedConnection RegisterNewDocumentButtonDownHandler(DocumentControlSignal::slot_type handler);
	ScopedConnection RegisterOpenDocumentButtonDownHandler(DocumentStringPathSignal::slot_type handler);
	ScopedConnection RegisterSaveDocumentButtonDownHandler(DocumentControlSignal::slot_type handler);
	ScopedConnection RegisterSaveAsDocumentButtonDownHandler(DocumentStringPathSignal::slot_type handler);
	ScopedConnection RegisterUndoButtonDownHandler(DocumentControlSignal::slot_type handler);
	ScopedConnection RegisterRedoButtonDownHandler(DocumentControlSignal::slot_type handler);
	ScopedConnection RegisterEllipseButtonDownHandler(DocumentControlSignal::slot_type handler);
	ScopedConnection RegisterRectangleButtonDownHandler(DocumentControlSignal::slot_type handler);
	ScopedConnection RegisterTriangleButtonDownHandler(DocumentControlSignal::slot_type handler);
	ScopedConnection RegisterCrossButtonDownHandler(DocumentControlSignal::slot_type handler);
	ScopedConnection RegisterHasDocumentModifiedHandler(DocumentStateQuerySignal::slot_type handler);
	ScopedConnection RegisterHasDocumentPathHandler(DocumentStateQuerySignal::slot_type handler);
	ScopedConnection RegisterKeyPressedSignalHandler(WindowKeyboardEventSignal::slot_type handler);

private:
	void OnNewDocumentButtonDown(wxRibbonToolBarEvent& event);
	void OnOpenDocumentButtonDown(wxRibbonToolBarEvent& event);
	void OnSaveDocumentButtonDown(wxRibbonToolBarEvent& event);
	void OnSaveAsDocumentButtonDown(wxRibbonToolBarEvent& event);
	void OnExitButtonDown(wxRibbonToolBarEvent& event);

	void OnUndoActionButtonDown(wxRibbonToolBarEvent& event);
	void OnRedoActionButtonDown(wxRibbonToolBarEvent& event);

	void OnEllipseButtonDown(wxRibbonButtonBarEvent& event);
	void OnRectangleButtonDown(wxRibbonButtonBarEvent& event);
	void OnTriangleButtonDown(wxRibbonButtonBarEvent& event);
	void OnCrossButtonDown(wxRibbonButtonBarEvent& event);

	void OnRendererGdiplusButtonDown(wxRibbonButtonBarEvent& event);
	void OnRendererDirect2DButtonDown(wxRibbonButtonBarEvent& event);

	void OnResize(wxSizeEvent& event);
	void OnKeyboard(wxKeyEvent& event);
	void OnClose(wxCloseEvent& event);
	wxDECLARE_EVENT_TABLE();

	bool OpenSaveAsDialog();
	bool OpenSaveUnsavedChangesPrompt();

	void PerformThrowableActionSafely(const std::function<void()>& action);

	wxRibbonToolBar* m_undoRedoToolbar;
	ShapeEditorView* m_shapeEditorView;

	DocumentControlSignal m_newDocumentButtonDownSignal;
	DocumentStringPathSignal m_openDocumentButtonDownSignal;
	DocumentControlSignal m_saveDocumentButtonDownSignal;
	DocumentStringPathSignal m_saveAsDocumentButtonDownSignal;
	DocumentControlSignal m_undoButtonDownSignal;
	DocumentControlSignal m_redoButtonDownSignal;
	DocumentControlSignal m_ellipseButtonDownSignal;
	DocumentControlSignal m_rectangleButtonDownSignal;
	DocumentControlSignal m_triangleButtonDownSignal;
	DocumentControlSignal m_crossButtonDownSignal;
	DocumentStateQuerySignal m_hasDocumentModifiedSignal;
	DocumentStateQuerySignal m_hasDocumentPathSignal;
	WindowKeyboardEventSignal m_keyPressedSignal;
};
