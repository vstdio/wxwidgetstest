#pragma once
#include "IRenderer.h"
#include <wx/defs.h> // WXHDC
#include <memory>

class GdiplusRenderer : public IRenderer
{
public:
	GdiplusRenderer(WXHDC wxHandleDeviceContext);
	~GdiplusRenderer();

	// Inherited via IRenderer
	void StrokeRect(const glm::vec4& rect, const glm::vec3& color, float width = 1.f)override;
	void FillRect(const glm::vec4& rect, const glm::vec3& color)override;

	void StrokeEllipse(const glm::vec4& rect, const glm::vec3& color, float width = 1.f)override;
	void FillEllipse(const glm::vec4& rect, const glm::vec3& color)override;

	void StrokePolygon(const std::vector<glm::vec2>& points, const glm::vec3& color, float width = 1.f)override;
	void FillPolygon(const std::vector<glm::vec2>& points, const glm::vec3& color)override;

	void Clear(const glm::vec3& color)override;

private:
	class Impl;
	std::unique_ptr<Impl> m_pImpl;
};
