#include "stdafx.h"
#include "InsertShapeCommand.h"
#include "UnpickNodeCommand.h"
#include "ShapeScene.h"
#include "TriangleNode.h"
#include "RectangleNode.h"
#include "EllipseNode.h"

namespace
{

const glm::vec4 DEFAULT_BOUNDING_RECT = { 0, 0, 150, 150 };

}

InsertShapeCommand::InsertShapeCommand(ShapeNodeType type)
	: m_type(type)
{
}

void InsertShapeCommand::Undo(IShapeSceneCommandControl& scene)
{
	scene.DeleteLastInsertedNode();
}

void InsertShapeCommand::Redo(IShapeSceneCommandControl& scene)
{
	Execute(scene);
}

bool InsertShapeCommand::Execute(IShapeSceneCommandControl& scene)
{
	const glm::vec2 center = {
		scene.GetSize().x / 2 - DEFAULT_BOUNDING_RECT.z / 2,
		scene.GetSize().y / 2 - DEFAULT_BOUNDING_RECT.w / 2
	};

	switch (m_type)
	{
	case ShapeNodeType::Rectangle:
		scene.InsertNode(std::make_unique<RectangleNode>(DEFAULT_BOUNDING_RECT));
		break;
	case ShapeNodeType::Ellipse:
		scene.InsertNode(std::make_unique<EllipseNode>(DEFAULT_BOUNDING_RECT));
		break;
	case ShapeNodeType::Triangle:
		scene.InsertNode(std::make_unique<TriangleNode>(DEFAULT_BOUNDING_RECT));
		break;
	}

	const size_t lastInsertedNodeIndex = scene.GetShapesCount() - 1;
	ShapeNode* lastInsertedNodePtr = scene.GetShape(lastInsertedNodeIndex);
	assert(lastInsertedNodePtr != nullptr);
	lastInsertedNodePtr->MoveTo(center);

	return true;
}

std::string InsertShapeCommand::GetName()const
{
	return "Insert shape";
}

bool InsertShapeCommand::Merge(IShapeCommand& other)
{
	(void)other;
	return false;
}
