#pragma once
#include "ForwardDeclarations.h"
#include "Aliases.h"
#include <memory>

class AppWindowController
{
public:
	AppWindowController(AppWindow& appWindow);
	~AppWindowController();

private:
	AppWindow& m_appWindow;
	std::shared_ptr<ShapeScene> m_shapeScene;
	std::unique_ptr<ShapeSceneController> m_sceneController;

	ScopedConnection m_newDocumentButtonDownSignalConnection;
	ScopedConnection m_openDocumentButtonDownSignalConnection;
	ScopedConnection m_saveDocumentButtonDownSignalConnection;
	ScopedConnection m_saveAsDocumentButtonDownSignalConnection;
	ScopedConnection m_undoButtonDownSignalConnection;
	ScopedConnection m_redoButtonDownSignalConnection;
	ScopedConnection m_ellipseButtonDownSignalConnection;
	ScopedConnection m_rectangleButtonDownSignalConnection;
	ScopedConnection m_triangleButtonDownSignalConnection;
	ScopedConnection m_crossButtonDownSignalConnection;
	ScopedConnection m_hasDocumentModifiedSignalConnection;
	ScopedConnection m_hasDocumentPathSignalConnection;
	ScopedConnection m_keyPressedSignalConnection;
};
