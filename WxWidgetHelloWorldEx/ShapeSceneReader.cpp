#include "stdafx.h"
#include "ShapeSceneReader.h"
#include "ShapeNode.h"
#include "RectangleNode.h"
#include "TriangleNode.h"
#include "EllipseNode.h"
#include <tinyxml2.h>

namespace
{
	std::unique_ptr<ShapeNode> CreateShape(const std::string& type, const glm::vec4& boundingRect, bool inversed)
	{
		if (type == "Rectangle")
		{
			return std::make_unique<RectangleNode>(boundingRect, inversed);
		}
		if (type == "Ellipse")
		{
			return std::make_unique<EllipseNode>(boundingRect, inversed);
		}
		if (type == "Triangle")
		{
			return std::make_unique<TriangleNode>(boundingRect, inversed);
		}
		throw std::runtime_error("CreateShape(): document have unknown shape type '" + type + "'");
	}

	glm::vec4 ReadShapePropertyBoundingRect(tinyxml2::XMLElement* element)
	{
		assert(element != nullptr);
		glm::vec4 boundingRect;

		auto readIntoRectPropertyFn = [element](const std::string& propertyName, float& rectProperty)
		{
			auto status = element->QueryFloatAttribute(propertyName.c_str(), &rectProperty);
			if (status != tinyxml2::XML_SUCCESS)
			{
				const std::string message = "bad shape scene document: can't read bounding rect property";
				throw std::runtime_error(message);
			}
		};

		readIntoRectPropertyFn("x", boundingRect.x);
		readIntoRectPropertyFn("y", boundingRect.y);
		readIntoRectPropertyFn("width", boundingRect.z);
		readIntoRectPropertyFn("height", boundingRect.w);

		return boundingRect; // if there is no exception thrown, we are assuming that rect initialized correctly
	}

	bool ReadShapePropertyIsInversed(tinyxml2::XMLElement* element)
	{
		assert(element != nullptr);
		bool inversed;
		auto status = element->QueryBoolAttribute("inversed", &inversed);
		if (status != tinyxml2::XML_SUCCESS)
		{
			const std::string message = "bad shape scene document: can't read inversed property";
			throw std::runtime_error(message);
		}
		return inversed; // if there is no exception thrown, we are assuming that boolean variable initialized correctly
	}
}

ShapeSceneReader::ShapeSceneReader(const std::string& filePath)
	: m_filePath(filePath)
{
}

std::vector<std::unique_ptr<ShapeNode>> ShapeSceneReader::GetShapes()
{
	std::vector<std::unique_ptr<ShapeNode>> shapes;

	tinyxml2::XMLDocument document;
	if (document.LoadFile(m_filePath.c_str()) != tinyxml2::XML_SUCCESS)
	{
		const std::string message = "bad shape scene document: can't parse as XML";
		throw std::runtime_error(message);
	}

	tinyxml2::XMLNode* root = document.FirstChild();
	if (root == nullptr)
	{
		const std::string message = "bad shape scene document: can't find XML root";
		throw std::runtime_error(message);
	}

	tinyxml2::XMLElement* element = root->FirstChildElement("Shape");
	while (element != nullptr)
	{
		const glm::vec4 boundingRect = ReadShapePropertyBoundingRect(element);
		const bool inversed = ReadShapePropertyIsInversed(element);
		shapes.push_back(CreateShape(element->GetText(), boundingRect, inversed));
		element = element->NextSiblingElement("Shape");
	}

	return shapes;
}
