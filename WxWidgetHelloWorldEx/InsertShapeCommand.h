#pragma once

#include "IShapeCommand.h"
#include "ForwardDeclarations.h"

class InsertShapeCommand : public IShapeCommand
{
public:
	InsertShapeCommand(ShapeNodeType type);

	void Undo(IShapeSceneCommandControl& scene)override;
	void Redo(IShapeSceneCommandControl& scene)override;
	bool Execute(IShapeSceneCommandControl& scene)override;
	std::string GetName()const override;
	bool Merge(IShapeCommand& other)override;

private:
	ShapeNodeType m_type;
};
