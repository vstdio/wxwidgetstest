#include "stdafx.h"
#include "MoveShapeCommand.h"
#include "PickNodeCommand.h"
#include "UnpickNodeCommand.h"
#include "ResizeShapeCommand.h"
#include "ShapeScene.h"
#include "ShapeNode.h"
#include "RectangleNode.h"
#include "TriangleNode.h"
#include "EllipseNode.h"
#include "ShapeEditorView.h"
#include "ShapeSceneSerializer.h"
#include "ShapeSceneReader.h"
#include "Constants.h"

ShapeScene::ShapeScene(const glm::ivec2& size, std::vector<ShapeNodePtr> && shapes)
	: m_size(size)
	, m_dragOffset(glm::vec2(0, 0))
	, m_shapeBounds(*this)
	, m_commandManager(*this)
	, m_shapes(std::move(shapes))
	, m_pickedNodeIndex(NO_PICKED_NODE_INDEX)
	, m_isDragging(false)
	, m_isResizing(false)
	, m_commandSignal()
	, m_setCursorSignal()
{
}

void ShapeScene::SerializeToFile(const std::string& path)const
{
	ShapeSceneSerializer serializer(path);
	for (const auto& shape : m_shapes)
	{
		serializer.SerializeNode(*shape);
	}
	serializer.Flush();
}

void ShapeScene::LoadFromFile(const std::string& path)
{
	Clear();
	ShapeSceneReader reader(path);
	m_shapes = std::move(reader.GetShapes());
}

void ShapeScene::OnCommand(ShapeCommandPtr command)
{
	bool saved = false;
	// ��������� �������, ���� �� �� ���� �������������
	if (command->Execute(*this))
	{
		m_commandManager.SaveCommand(std::move(command));
		saved = true;
	}
	// �������� ������ ����, ��� ���� ����������� ������� ���������� �������
	// ������� ������������. � ����� ������� �������� �������� ����� ���������
	// commandManager.
	m_commandSignal(saved);
}

void ShapeScene::OnUndo()
{
	m_commandManager.Undo();
}

void ShapeScene::OnRedo()
{
	m_commandManager.Redo();
}

void ShapeScene::Clear()
{
	m_shapes.clear();
	m_pickedNodeIndex = NO_PICKED_NODE_INDEX;
	m_shapeBounds.UnhitBounds();
	m_shapeBounds.SetShape(nullptr);
	m_dragOffset = glm::vec2(0, 0);
	m_commandManager.Clear();
}

void ShapeScene::InsertNode(ShapeNodePtr node)
{
	m_shapes.push_back(std::move(node));
}

void ShapeScene::InsertNodeAt(ShapeNodePtr node, size_t pos)
{
	auto it = m_shapes.insert(m_shapes.begin() + pos, std::move(node));
	assert(it != m_shapes.end());
}

ShapeNode* ShapeScene::GetShape(size_t index)
{
	if (index >= 0 && index < m_shapes.size())
	{
		return m_shapes[index].get();
	}
	return nullptr;
}

const ShapeNode* ShapeScene::GetShape(size_t index)const
{
	if (index >= 0 && index < m_shapes.size())
	{
		return m_shapes[index].get();
	}
	return nullptr;
}

// ����� ����� ������ ������ ������� CPickNodeCommand
void ShapeScene::PickNode(size_t index)
{
	assert(index >= 0 && index < m_shapes.size());
	m_pickedNodeIndex = index;
	m_shapeBounds.SetShape(m_shapes[index].get());
}

// ��� ����� �������� ��� ������� ������
// �� ����� �� ������� ������� ����� ������
void ShapeScene::UnpickNode()
{
	m_shapeBounds.SetShape(nullptr);
	m_pickedNodeIndex = NO_PICKED_NODE_INDEX;
}

bool ShapeScene::IsNodePicked()const
{
	return m_pickedNodeIndex != NO_PICKED_NODE_INDEX;
}

int ShapeScene::GetPickedNodeIndex()const
{
	return m_pickedNodeIndex;
}

void ShapeScene::OnMouseLeftDown(const glm::vec2& pos)
{
	// ���� ������ ����� ���������� ������, ��� ������ �� ���������
	m_shapeBounds.SetHitBoundsType(m_shapeBounds.HitTest(pos));
	if (m_shapeBounds.IsBoundsHit())
	{
		return;
	}

	// �������� ������ ��� ������ � �������� ����� ��� ���������.
	// ���� ���������� ����� ������ ���������, ��������� ������� "��������"
	// � ���������� ������ ��� ����������� �����������,
	// ����� ��������� ������� "����� ���������"
	const int nodeIndexToPick = GetNodeIndexToPick(pos);
	if (nodeIndexToPick != NO_PICKED_NODE_INDEX)
	{
		OnCommand(std::make_unique<PickNodeCommand>(nodeIndexToPick));
		OnDragBegin(pos);
	}
	else
	{
		OnCommand(std::make_unique<UnpickNodeCommand>(m_pickedNodeIndex));
	}
}

void ShapeScene::OnMouseMotion(const glm::vec2& pos)
{
	// ���� ��� �������� �������� �������� �������, �������� ������
	// � ������������� ������� ������
	BoundsType boundsType = m_shapeBounds.HitTest(pos);
	if (boundsType != BoundsType::None)
	{
		m_setCursorSignal(boundsType);
	}

	// ���� ����� ���������� ������ ���� ������, ���������
	// ��������� �������� ������, ����� ������� �� �������
	if (m_shapeBounds.IsBoundsHit())
	{
		m_isResizing = true;
		OnCommand(std::make_unique<ResizeShapeCommand>(m_shapeBounds, pos));
		return;
	}

	// ���� ���������� ������ �� ������������ �������
	// � �� ��������� � ��������� m_isDragging, ���������� � =)
	if (m_pickedNodeIndex != NO_PICKED_NODE_INDEX && m_isDragging)
	{
		OnCommand(std::make_unique<MoveShapeCommand>(m_pickedNodeIndex, pos - m_dragOffset));
	}
}

void ShapeScene::OnMouseLeftUp(const glm::vec2& pos)
{
	(void)pos;
	m_isResizing = false;

	// ��������, ��� ���������� �� ��������� ������� ����������� �� �����
	// (����� ����� ������� ���������)
	m_commandManager.DisableMergeOnce();

	// ������� ���� ����, ��� ����� ���������� ������ ������
	m_shapeBounds.UnhitBounds();

	// �������� � ����� ����������� ������
	OnDragEnd();
}

void ShapeScene::OnMouseLeave(const glm::vec2& pos)
{
	// ������ ���� �����, ��� � ��� ���������� ����� ������ ����
	OnMouseLeftUp(pos);
}

void ShapeScene::Draw(IRenderer& renderer)const
{
	for (const auto& shape : m_shapes)
	{
		shape->Draw(renderer);
	}
	if (m_shapeBounds.IsShapeSet())
	{
		m_shapeBounds.Draw(renderer);
	}
}

CommandManager& ShapeScene::GetCommandManager()
{
	return m_commandManager;
}

const CommandManager& ShapeScene::GetCommandManager()const
{
	return m_commandManager;
}

ScopedConnection ShapeScene::RegisterCommandSignalHandler(PostCommandSignal::slot_type handler)
{
	return m_commandSignal.connect(handler);
}

ScopedConnection ShapeScene::RegisterSetCursorSignalHandler(BoundsHoverSignal::slot_type handler)
{
	return m_setCursorSignal.connect(handler);
}

glm::vec2 ShapeScene::GetSize()const
{
	return m_size;
}

void ShapeScene::SetSize(const glm::vec2& size)
{
	m_size = size;
}

size_t ShapeScene::GetShapesCount() const
{
	return m_shapes.size();
}

bool ShapeScene::IsResizing() const
{
	return m_isResizing;
}

std::optional<std::pair<size_t, ShapeNodePtr>> ShapeScene::DeletePickedNode()
{
	auto it = std::find_if(m_shapes.begin(), m_shapes.end(), [&](ShapeNodePtr& shape) {
		return shape.get() == m_shapeBounds.GetShape();
	});
	if (it != m_shapes.end())
	{
		// ����� ��������� ������ ������� � �� ���������
		// OnCommand(std::make_unique<UnpickNodeCommand>(m_pickedNodeIndex));
		UnpickNode();

		ShapeNodePtr shapeErasedFromVector = std::move(*it);
		const size_t nodeToDeleteIndex = std::distance(m_shapes.begin(), it);

		m_shapes.erase(it);
		return std::optional<std::pair<size_t, ShapeNodePtr>>(std::make_pair(nodeToDeleteIndex, std::move(shapeErasedFromVector)));
	}
	return std::nullopt;
}

ShapeNodePtr ShapeScene::DeleteNode(size_t index)
{
	if (index >= m_shapes.size())
	{
		return nullptr;
	}
	UnpickNode();
	ShapeNodePtr movedShape = std::move(m_shapes.at(index));
	m_shapes.erase(m_shapes.begin() + index);
	return movedShape;
}

void ShapeScene::DeleteLastInsertedNode()
{
	assert(!m_shapes.empty());
	m_shapes.pop_back();
}

int ShapeScene::GetNodeIndexToPick(const glm::vec2& pos)
{
	for (int index = int(m_shapes.size()) - 1; index >= 0; --index)
	{
		if (m_shapes[index]->HitTest(pos))
		{
			return index;
		}
	}
	return NO_PICKED_NODE_INDEX;
}

// ����� ����� ������ ��� ������� ������ �� ������
void ShapeScene::OnDragBegin(const glm::vec2& startPos)
{
	assert(m_pickedNodeIndex >= 0 && m_pickedNodeIndex < int(m_shapes.size()));
	m_dragOffset = startPos - m_shapes[m_pickedNodeIndex]->GetPosition();
	m_isDragging = true;
}

// ����� ����� ������ ��� ���������� ����� ������ �����
void ShapeScene::OnDragEnd()
{
	m_isDragging = false;
	m_dragOffset = glm::vec2(0, 0);
}
