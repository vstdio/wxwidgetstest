#pragma once
#include "ForwardDeclarations.h" // BoundsType, glm::ivec2&
#include <boost/signals2.hpp>
#include <string>

using ScopedConnection = boost::signals2::scoped_connection;

using MouseSignal = boost::signals2::signal<void(const glm::ivec2&)>;
using PostCommandSignal = boost::signals2::signal<void(bool)>;
using BoundsHoverSignal = boost::signals2::signal<void(BoundsType)>;

using DocumentControlSignal = boost::signals2::signal<void()>;
using DocumentStringPathSignal = boost::signals2::signal<void(const std::string&)>;
using DocumentStateQuerySignal = boost::signals2::signal<bool()>;
using WindowKeyboardEventSignal = boost::signals2::signal<void(int)>;
