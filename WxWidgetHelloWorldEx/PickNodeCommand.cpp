#include "stdafx.h"
#include "PickNodeCommand.h"
#include "ShapeScene.h"
#include "ShapeBounds.h"
#include "UnpickNodeCommand.h"
#include "Constants.h"

PickNodeCommand::PickNodeCommand(size_t index)
	: m_nodeIndexToPick(index)
	, m_alreadyPickedNode()
{
}

void PickNodeCommand::Undo(IShapeSceneCommandControl& scene)
{
	if (m_alreadyPickedNode)
	{
		scene.PickNode(m_alreadyPickedNode.value());
	}
	else
	{
		scene.UnpickNode();
	}
}

void PickNodeCommand::Redo(IShapeSceneCommandControl& scene)
{
	scene.PickNode(m_nodeIndexToPick);
}

bool PickNodeCommand::Execute(IShapeSceneCommandControl& scene)
{
	// ���� ������ ��� �������, ��� ������ ��������� ������ �������
	if (scene.GetPickedNodeIndex() == int(m_nodeIndexToPick))
	{
		return false;
	}

	// ���� ��� ������� �����-���� ������ ������, ������� � �� ��������� ������ ��������
	if (scene.IsNodePicked())
	{
		m_alreadyPickedNode = scene.GetPickedNodeIndex();
		/*const int pickedNodeIndex = scene.GetPickedNodeIndex();
		assert(pickedNodeIndex != NO_PICKED_NODE_INDEX);
		scene.OnCommand(std::make_unique<UnpickNodeCommand>(pickedNodeIndex));*/
	}

	scene.PickNode(m_nodeIndexToPick);
	assert(scene.IsNodePicked());
	return scene.IsNodePicked();
}

std::string PickNodeCommand::GetName()const
{
	return "Pick node";
}

bool PickNodeCommand::Merge(IShapeCommand& other)
{
	(void)other;
	return false;
}
