#pragma once
#include "IRenderer.h"
#include <memory>

class D2DRendererDC : public IRenderer
{
public:
	D2DRendererDC();
	~D2DRendererDC();

	void BeginDraw(const glm::ivec2& offset, wxPaintDC& dc);
	void EndDraw();

	void StrokeRect(const glm::vec4& rect, const glm::vec3& color, float width = 1.f) override;
	void FillRect(const glm::vec4& rect, const glm::vec3& color) override;
	void StrokeEllipse(const glm::vec4& rect, const glm::vec3& color, float width = 1.f) override;
	void FillEllipse(const glm::vec4& rect, const glm::vec3 & color) override;
	void StrokePolygon(const std::vector<glm::vec2>& points, const glm::vec3& color, float width = 1.f) override;
	void FillPolygon(const std::vector<glm::vec2>& points, const glm::vec3& color) override;

	void Clear(const glm::vec3& color)override;

private:
	class Impl;
	std::unique_ptr<Impl> m_pImpl;
};
