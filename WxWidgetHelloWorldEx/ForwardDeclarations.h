#pragma once

namespace Gdiplus
{
	class Graphics;
}

class AppWindow;
class ShapeEditorView;
class ShapeScene;
class ShapeNode;
class ShapeBounds;
class ShapesDocument;
class IShapeCommand;
class ShapeSceneController;
class IRenderer;
class GdiplusRenderer;
class D2DRendererDC;

enum class ShapeNodeType
{
	Rectangle,
	Ellipse,
	Triangle
};

enum class BoundsType
{
	Left,
	Right,
	Up,
	Down,
	UpLeft,
	UpRight,
	DownLeft,
	DownRight,
	None
};
