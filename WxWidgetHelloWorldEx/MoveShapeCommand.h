#pragma once
#include "ForwardDeclarations.h"
#include "IShapeCommand.h"

class MoveShapeCommand : public IShapeCommand
{
public:
	MoveShapeCommand(size_t shapeIndex, const glm::vec2& newPos);

	void Undo(IShapeSceneCommandControl& scene)override;
	void Redo(IShapeSceneCommandControl& scene)override;
	bool Execute(IShapeSceneCommandControl& scene)override;
	std::string GetName()const override;
	bool Merge(IShapeCommand& other)override;

	glm::vec2 GetNewPos()const;
	glm::vec2 GetOldPos()const;

private:
	size_t m_shapeIndex;
	glm::vec2 m_newPos;
	glm::vec2 m_oldPos;
};
