#include "stdafx.h"
#include "AppWindow.h"
#include "InsertShapeCommand.h"
#include "DeleteShapeCommand.h"
#include "PickNodeCommand.h"
#include "Constants.h"

#include "D2DLogo.xpm"
#include "GDIPlusLogo.xpm"
#include "CircleLogo.xpm"
#include "RectangleLogo.xpm"
#include "TriangleLogo.xpm"
#include "CrossLogo.xpm"

namespace
{

enum
{
	ID_UNDO_REDO_RIBBON_TOOLBAR = 0,
	ID_ADD_RECTANGLE_BUTTON,
	ID_ADD_ELLIPSE_BUTTON,
	ID_ADD_TRIANGLE_BUTTON,
	ID_CLEAR_SCENE_CROSS_BUTTON,
	ID_RENDERER_GDIPLUS_TOGGLE_BUTTON,
	ID_RENDERER_DIRECT2D_TOGGLE_BUTTON
};

const glm::ivec2 SHAPE_EDITOR_VIEW_SCROLL_RATE = { 50, 50 };

}

AppWindow::AppWindow(const wxString& title, const wxPoint& position, const wxSize& size)
	: wxFrame(nullptr, wxID_ANY, title, position, size)
	, m_shapeEditorView(nullptr)
	, m_undoRedoToolbar(nullptr)
	, m_newDocumentButtonDownSignal()
	, m_openDocumentButtonDownSignal()
	, m_saveDocumentButtonDownSignal()
	, m_saveAsDocumentButtonDownSignal()
	, m_undoButtonDownSignal()
	, m_redoButtonDownSignal()
	, m_ellipseButtonDownSignal()
	, m_rectangleButtonDownSignal()
	, m_triangleButtonDownSignal()
	, m_hasDocumentModifiedSignal()
	, m_hasDocumentPathSignal()
	, m_keyPressedSignal()
{
	wxRibbonBar* ribbon = new wxRibbonBar(this, wxID_ANY, wxDefaultPosition, wxDefaultSize,
		wxRIBBON_BAR_FLOW_HORIZONTAL | wxRIBBON_BAR_SHOW_PAGE_LABELS | wxRIBBON_BAR_SHOW_PAGE_ICONS |
		wxRIBBON_BAR_SHOW_PAGE_LABELS | wxRIBBON_BAR_SHOW_PANEL_EXT_BUTTONS);

	// FILE PAGE
	wxRibbonPage* fileRibbonPage = new wxRibbonPage(ribbon, wxID_ANY, wxT("FILE"));
	{
		wxRibbonPanel *toolbarDocumentPanel = new wxRibbonPanel(fileRibbonPage, wxID_ANY, wxT("Document"),
			wxNullBitmap, wxDefaultPosition, wxDefaultSize,
			wxRIBBON_PANEL_NO_AUTO_MINIMISE);

		wxRibbonToolBar* m_documentToolbar = new wxRibbonToolBar(toolbarDocumentPanel, wxID_ANY);
		m_documentToolbar->AddTool(wxID_NEW, wxArtProvider::GetBitmap(wxART_NEW, wxART_OTHER, wxSize(32, 32)), "New");
		m_documentToolbar->AddSeparator();
		m_documentToolbar->AddTool(wxID_OPEN, wxArtProvider::GetBitmap(wxART_FILE_OPEN, wxART_OTHER, wxSize(32, 32)), "Open");
		m_documentToolbar->AddTool(wxID_SAVE, wxArtProvider::GetBitmap(wxART_FILE_SAVE, wxART_OTHER, wxSize(32, 32)), "Save");
		m_documentToolbar->AddTool(wxID_SAVEAS, wxArtProvider::GetBitmap(wxART_FILE_SAVE_AS, wxART_OTHER, wxSize(32, 32)), "Save as");
		m_documentToolbar->AddSeparator();
		m_documentToolbar->AddTool(wxID_EXIT, wxArtProvider::GetBitmap(wxART_QUIT, wxART_OTHER, wxSize(32, 32)), "Exit");
	}

	// VIEW PAGE
	wxRibbonPage* viewPage = new wxRibbonPage(ribbon, wxID_ANY, "VIEW");
	{
		wxRibbonPanel* shapesPanel = new wxRibbonPanel(viewPage, wxID_ANY, wxT("Scene"), wxNullBitmap);
		wxRibbonButtonBar* shapesButtonBar = new wxRibbonButtonBar(shapesPanel);
		shapesButtonBar->AddButton(ID_ADD_RECTANGLE_BUTTON, "Rectangle", wxBitmap(RECTANGLE_RAW_BITMAP_DATA));
		shapesButtonBar->AddButton(ID_ADD_ELLIPSE_BUTTON, "Ellipse", wxBitmap(CIRCLE_RAW_BITMAP_DATA));
		shapesButtonBar->AddButton(ID_ADD_TRIANGLE_BUTTON, "Triangle", wxBitmap(TRIANGLE_RAW_BITMAP_DATA));
		shapesButtonBar->AddButton(ID_CLEAR_SCENE_CROSS_BUTTON, "Clear", wxBitmap(CROSS_RAW_BITMAP_DATA));

		wxRibbonPanel* commandsPanel = new wxRibbonPanel(viewPage, wxID_ANY, wxT("Commands"), wxNullBitmap);

		m_undoRedoToolbar = new wxRibbonToolBar(commandsPanel, ID_UNDO_REDO_RIBBON_TOOLBAR);
		m_undoRedoToolbar->AddTool(wxID_UNDO, wxArtProvider::GetBitmap(wxART_UNDO, wxART_OTHER, wxSize(32, 32)), "Undo");
		m_undoRedoToolbar->AddTool(wxID_REDO, wxArtProvider::GetBitmap(wxART_REDO, wxART_OTHER, wxSize(32, 32)), "Redo");

		m_undoRedoToolbar->EnableTool(wxID_UNDO, false);
		m_undoRedoToolbar->EnableTool(wxID_REDO, false);

		wxRibbonPanel* rendererSelectionPanel = new wxRibbonPanel(viewPage, wxID_ANY, wxT("Rendering Mode"));
		wxRibbonButtonBar* rendererSelectionButtonBar = new wxRibbonButtonBar(rendererSelectionPanel);

		rendererSelectionButtonBar->AddButton(ID_RENDERER_GDIPLUS_TOGGLE_BUTTON, "GDI+", wxBitmap(GDI_PLUS_LOGO_XPM),
			"Pick GDI+ Rendering Mode (No Anti-Aliasing).");
		rendererSelectionButtonBar->AddButton(ID_RENDERER_DIRECT2D_TOGGLE_BUTTON, "Direct2D", wxBitmap(D2D_LOGO_XPM),
			"Pick Direct2D Rendering Mode.");

		rendererSelectionButtonBar->ToggleButton(ID_RENDERER_GDIPLUS_TOGGLE_BUTTON, true); // GDI+ is set by default
	}

	ribbon->Realize();

	m_shapeEditorView = new ShapeEditorView(this);

	wxFrame::Bind(wxEVT_CHAR_HOOK, &AppWindow::OnKeyboard, this);

	wxSizer* sizer = new wxBoxSizer(wxVERTICAL);
	sizer->Add(ribbon, 0, wxEXPAND);
	sizer->Add(m_shapeEditorView, 1, wxEXPAND);

	wxFrame::SetSizer(sizer);
	wxFrame::SetAutoLayout(true);

	wxFrame::CreateStatusBar();
	wxFrame::SetStatusText(
		(boost::format(DEFAULT_STATUSBAR_MESSAGE) %
			ToString(m_shapeEditorView->GetRendererType())).str());

	const glm::ivec2 editFieldSize = { m_shapeEditorView->GetSize().GetWidth(), m_shapeEditorView->GetSize().GetHeight() };

	m_shapeEditorView->SetScrollRate(SHAPE_EDITOR_VIEW_SCROLL_RATE.x, SHAPE_EDITOR_VIEW_SCROLL_RATE.y);
	m_shapeEditorView->SetVirtualSize(editFieldSize.x, editFieldSize.y);
	m_shapeEditorView->SetScrollbars(1, 1, editFieldSize.x, editFieldSize.y, 0, false);

	wxFrame::SetIcon(wxICON(WXICON_AAA));
}

wxRibbonToolBar& AppWindow::GetUndoRedoToolbar()
{
	assert(m_undoRedoToolbar != nullptr);
	return *m_undoRedoToolbar;
}

const wxRibbonToolBar& AppWindow::GetUndoRedoToolbar()const
{
	assert(m_undoRedoToolbar != nullptr);
	return *m_undoRedoToolbar;
}

ShapeEditorView& AppWindow::GetShapeEditorView()
{
	assert(m_shapeEditorView != nullptr);
	return *m_shapeEditorView;
}

const ShapeEditorView& AppWindow::GetShapeEditorView()const
{
	assert(m_shapeEditorView != nullptr);
	return *m_shapeEditorView;
}

ScopedConnection AppWindow::RegisterNewDocumentButtonDownHandler(DocumentControlSignal::slot_type handler)
{
	return m_newDocumentButtonDownSignal.connect(handler);
}

ScopedConnection AppWindow::RegisterOpenDocumentButtonDownHandler(DocumentStringPathSignal::slot_type handler)
{
	return m_openDocumentButtonDownSignal.connect(handler);
}

ScopedConnection AppWindow::RegisterSaveDocumentButtonDownHandler(DocumentControlSignal::slot_type handler)
{
	return m_saveDocumentButtonDownSignal.connect(handler);
}

ScopedConnection AppWindow::RegisterSaveAsDocumentButtonDownHandler(DocumentStringPathSignal::slot_type handler)
{
	return m_saveAsDocumentButtonDownSignal.connect(handler);
}

ScopedConnection AppWindow::RegisterUndoButtonDownHandler(DocumentControlSignal::slot_type handler)
{
	return m_undoButtonDownSignal.connect(handler);
}

ScopedConnection AppWindow::RegisterRedoButtonDownHandler(DocumentControlSignal::slot_type handler)
{
	return m_redoButtonDownSignal.connect(handler);
}

ScopedConnection AppWindow::RegisterEllipseButtonDownHandler(DocumentControlSignal::slot_type handler)
{
	return m_ellipseButtonDownSignal.connect(handler);
}

ScopedConnection AppWindow::RegisterRectangleButtonDownHandler(DocumentControlSignal::slot_type handler)
{
	return m_rectangleButtonDownSignal.connect(handler);
}

ScopedConnection AppWindow::RegisterTriangleButtonDownHandler(DocumentControlSignal::slot_type handler)
{
	return m_triangleButtonDownSignal.connect(handler);
}

ScopedConnection AppWindow::RegisterCrossButtonDownHandler(DocumentControlSignal::slot_type handler)
{
	return m_crossButtonDownSignal.connect(handler);
}

ScopedConnection AppWindow::RegisterHasDocumentModifiedHandler(DocumentStateQuerySignal::slot_type handler)
{
	return m_hasDocumentModifiedSignal.connect(handler);
}

ScopedConnection AppWindow::RegisterHasDocumentPathHandler(DocumentStateQuerySignal::slot_type handler)
{
	return m_hasDocumentPathSignal.connect(handler);
}

ScopedConnection AppWindow::RegisterKeyPressedSignalHandler(WindowKeyboardEventSignal::slot_type handler)
{
	return m_keyPressedSignal.connect(handler);
}

void AppWindow::OnNewDocumentButtonDown(wxRibbonToolBarEvent& WXUNUSED(event))
{
	if (!OpenSaveUnsavedChangesPrompt())
	{
		return;
	}

	PerformThrowableActionSafely([this]() {
		m_newDocumentButtonDownSignal();
	});
}

void AppWindow::OnOpenDocumentButtonDown(wxRibbonToolBarEvent& WXUNUSED(event))
{
	if (!OpenSaveUnsavedChangesPrompt())
	{
		return;
	}

	wxFileDialog* pOpenFileDialog = new wxFileDialog(this,
		_("Choose a file to open"), wxEmptyString, wxEmptyString,
		_("XML document|*.xml"), wxFD_OPEN, wxDefaultPosition);

	if (pOpenFileDialog->ShowModal() == wxID_OK)
	{
		// possible memory leak
		PerformThrowableActionSafely([&]() {
			m_openDocumentButtonDownSignal(pOpenFileDialog->GetPath().ToStdString());
		});
	}

	pOpenFileDialog->Destroy();
}

void AppWindow::OnSaveDocumentButtonDown(wxRibbonToolBarEvent& WXUNUSED(event))
{
	if (*m_hasDocumentPathSignal())
	{
		PerformThrowableActionSafely([this]() {
			m_saveDocumentButtonDownSignal();
		});
		return;
	}
	OpenSaveAsDialog();
}

void AppWindow::OnSaveAsDocumentButtonDown(wxRibbonToolBarEvent& WXUNUSED(event))
{
	OpenSaveAsDialog();
}

void AppWindow::OnUndoActionButtonDown(wxRibbonToolBarEvent& WXUNUSED(event))
{
	PerformThrowableActionSafely([this]() {
		m_undoButtonDownSignal();
	});
}

void AppWindow::OnRedoActionButtonDown(wxRibbonToolBarEvent& WXUNUSED(event))
{
	PerformThrowableActionSafely([this]() {
		m_redoButtonDownSignal();
	});
}

void AppWindow::OnEllipseButtonDown(wxRibbonButtonBarEvent& WXUNUSED(event))
{
	PerformThrowableActionSafely([this]() {
		m_ellipseButtonDownSignal();
	});
}

void AppWindow::OnRectangleButtonDown(wxRibbonButtonBarEvent& WXUNUSED(event))
{
	PerformThrowableActionSafely([this]() {
		m_rectangleButtonDownSignal();
	});
}

void AppWindow::OnTriangleButtonDown(wxRibbonButtonBarEvent& WXUNUSED(event))
{
	PerformThrowableActionSafely([this]() {
		m_triangleButtonDownSignal();
	});
}

void AppWindow::OnCrossButtonDown(wxRibbonButtonBarEvent& WXUNUSED(event))
{
	PerformThrowableActionSafely([this]() {
		m_crossButtonDownSignal();
	});
}

void AppWindow::OnRendererGdiplusButtonDown(wxRibbonButtonBarEvent& WXUNUSED(event))
{
	m_shapeEditorView->SetRendererType(RendererType::Gdiplus);
	wxFrame::SetStatusText(
		(boost::format(DEFAULT_STATUSBAR_MESSAGE) %
			ToString(m_shapeEditorView->GetRendererType())).str());

	// Redraw everything.
	m_shapeEditorView->Refresh(true);
}

void AppWindow::OnRendererDirect2DButtonDown(wxRibbonButtonBarEvent& WXUNUSED(event))
{
	m_shapeEditorView->SetRendererType(RendererType::Direct2D);
	wxFrame::SetStatusText(
		(boost::format(DEFAULT_STATUSBAR_MESSAGE) %
			ToString(m_shapeEditorView->GetRendererType())).str());

	// Redraw everything.
	m_shapeEditorView->Refresh(true);
}

void AppWindow::OnResize(wxSizeEvent& WXUNUSED(event))
{
	wxFrame::Layout();
}

void AppWindow::OnKeyboard(wxKeyEvent& event)
{
	PerformThrowableActionSafely([&]() {
		m_keyPressedSignal(event.GetKeyCode());
	});
}

void AppWindow::OnExitButtonDown(wxRibbonToolBarEvent& WXUNUSED(event))
{
	wxFrame::Close(true);
}

void AppWindow::OnClose(wxCloseEvent& event)
{
	if (!OpenSaveUnsavedChangesPrompt())
	{
		return;
	}
	event.Skip();
}

bool AppWindow::OpenSaveAsDialog()
{
	wxFileDialog* pSaveFileAsDialog = new wxFileDialog(
		this, _("Save File"), wxEmptyString, wxEmptyString,
		_("XML Document (*.xml)|*.xml"), wxFD_SAVE | wxFD_OVERWRITE_PROMPT, wxDefaultPosition);

	if (pSaveFileAsDialog->ShowModal() == wxID_OK)
	{
		m_saveAsDocumentButtonDownSignal(pSaveFileAsDialog->GetPath().ToStdString());
		pSaveFileAsDialog->Destroy();
		return true;
	}

	pSaveFileAsDialog->Destroy();
	return false;
}

// ���������� false ������ � ��� ������, ���� ���� ������� ����������� � ����������
// ���������, � ���� ������ ������ ��� ������ ����� ��� ����������
bool AppWindow::OpenSaveUnsavedChangesPrompt()
{
	if (*m_hasDocumentModifiedSignal())
	{
		wxMessageDialog *dial = new wxMessageDialog(NULL,
			wxT("There are some unsaved changes. Save?"), wxT("Question"),
			wxYES_NO | wxYES_DEFAULT | wxICON_QUESTION);
		if (dial->ShowModal() == wxID_YES)
		{
			return OpenSaveAsDialog();
		}
	}
	return true;
}

void AppWindow::PerformThrowableActionSafely(const std::function<void()>& action)
{
	try
	{
		action();
	}
	catch (const std::exception& ex)
	{
		wxMessageBox(wxString("Exception caught: '") + ex.what() + "'.", "Some caption string");
	}
	catch (...)
	{
		wxMessageBox("Unknown error occurred. Please, try again later.");
	}
}

wxBEGIN_EVENT_TABLE(AppWindow, wxFrame)
	EVT_RIBBONTOOLBAR_CLICKED(wxID_NEW, AppWindow::OnNewDocumentButtonDown)
	EVT_RIBBONTOOLBAR_CLICKED(wxID_OPEN, AppWindow::OnOpenDocumentButtonDown)
	EVT_RIBBONTOOLBAR_CLICKED(wxID_SAVE, AppWindow::OnSaveDocumentButtonDown)
	EVT_RIBBONTOOLBAR_CLICKED(wxID_SAVEAS, AppWindow::OnSaveAsDocumentButtonDown)
	EVT_RIBBONTOOLBAR_CLICKED(wxID_UNDO, AppWindow::OnUndoActionButtonDown)
	EVT_RIBBONTOOLBAR_CLICKED(wxID_REDO, AppWindow::OnRedoActionButtonDown)
	EVT_RIBBONTOOLBAR_CLICKED(wxID_EXIT, AppWindow::OnExitButtonDown)
	EVT_RIBBONBUTTONBAR_CLICKED(ID_ADD_RECTANGLE_BUTTON, AppWindow::OnRectangleButtonDown)
	EVT_RIBBONBUTTONBAR_CLICKED(ID_ADD_ELLIPSE_BUTTON, AppWindow::OnEllipseButtonDown)
	EVT_RIBBONBUTTONBAR_CLICKED(ID_ADD_TRIANGLE_BUTTON, AppWindow::OnTriangleButtonDown)
	EVT_RIBBONBUTTONBAR_CLICKED(ID_CLEAR_SCENE_CROSS_BUTTON, AppWindow::OnCrossButtonDown)
	EVT_RIBBONBUTTONBAR_CLICKED(ID_RENDERER_GDIPLUS_TOGGLE_BUTTON, AppWindow::OnRendererGdiplusButtonDown)
	EVT_RIBBONBUTTONBAR_CLICKED(ID_RENDERER_DIRECT2D_TOGGLE_BUTTON, AppWindow::OnRendererDirect2DButtonDown)
	EVT_SIZE(AppWindow::OnResize)
	EVT_KEY_DOWN(AppWindow::OnKeyboard)
	EVT_CLOSE(AppWindow::OnClose)
wxEND_EVENT_TABLE()
