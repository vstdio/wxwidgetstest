#pragma once
#include <wx/position.h>
#include "ShapeNode.h"

class RectangleNode : public ShapeNode
{
public:
	explicit RectangleNode(const glm::vec4& boundingRect, bool inversed = false);
	void Draw(IRenderer& renderer)const override;
	bool HitTest(const glm::vec2& point)const override;
	ShapeNodeType GetType()const;
};
