#pragma once
#include "ForwardDeclarations.h"
#include "IShapeSceneCommandControl.h"
#include <string>

class IShapeCommand
{
public:
	virtual ~IShapeCommand() = default;
	virtual void Undo(IShapeSceneCommandControl& scene) = 0;
	virtual void Redo(IShapeSceneCommandControl& scene) = 0;
	virtual bool Execute(IShapeSceneCommandControl& scene) = 0;
	virtual std::string GetName()const = 0;
	virtual bool Merge(IShapeCommand& other) = 0;
};
