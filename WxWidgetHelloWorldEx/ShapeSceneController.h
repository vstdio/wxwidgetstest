#pragma once
#include "ForwardDeclarations.h"
#include "ShapesDocument.h"
#include "Aliases.h"

class ShapeSceneController
{
public:
	ShapeSceneController(ShapeScene& shapeScene, ShapeEditorView& shapeEditorView);

	void OnUndoButtonDown();
	void OnRedoButtonDown();
	void OnCrossButtonDown();

	void OnAddRectangleButtonDown();
	void OnAddEllipseButtonDown();
	void OnAddTriangleButtonDown();

	void OnSaveAs(const std::string& path);
	bool HasDocumentModifed()const;
	bool HasDocumentPath()const;
	void OnNewDocument();
	void OnOpen(const std::string& path);
	void OnSave();

private:
	void PostCommandExecute(bool commandSaved);
	void OnSetCursorSignal(BoundsType boundsType);

	void OnMouseLeftDown(const glm::ivec2& pos);
	void OnMouseMotion(const glm::ivec2& pos);
	void OnMouseLeftUp(const glm::ivec2& pos);
	void OnMouseLeave(const glm::ivec2& pos);

	void UpdateView();

	ShapeScene& m_shapeScene;
	ShapeEditorView& m_shapeEditorView;
	ShapesDocument m_document;

	ScopedConnection m_commandSignalConnection;
	ScopedConnection m_setCursorSignalConnection;
	ScopedConnection m_mouseLeftDownSignalConnection;
	ScopedConnection m_mouseMotionSignalConnection;
	ScopedConnection m_mouseLeftUpSignalConnection;
	ScopedConnection m_mouseLeaveSignalConnection;
};
