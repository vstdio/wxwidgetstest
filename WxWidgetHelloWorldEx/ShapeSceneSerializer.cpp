#include "stdafx.h"
#include "ShapeSceneSerializer.h"
#include "ShapeNode.h"
#include <tinyxml2.h>

namespace
{
	const char* ToString(ShapeNodeType type)
	{
		switch (type)
		{
		case ShapeNodeType::Rectangle:
			return "Rectangle";
		case ShapeNodeType::Ellipse:
			return "Ellipse";
		case ShapeNodeType::Triangle:
			return "Triangle";
		default:
			throw std::logic_error("ToString(): switch branch is not implemented for that ShapeNodeType");
		}
	}
}

class ShapeSceneSerializer::Impl
{
public:
	ShapeSceneSerializer::Impl(const std::string& filePath)
		: m_filePath(filePath)
		, m_document()
		, m_pRoot(nullptr)
	{
		m_pRoot = m_document.NewElement("ShapeScene");
		m_document.InsertFirstChild(m_pRoot);
	}

	void SerializeNode(const ShapeNode& shape)
	{
		tinyxml2::XMLElement* pElement = m_document.NewElement("Shape");
		pElement->SetText(ToString(shape.GetType()));
		pElement->SetAttribute("x", shape.GetBoundingRect().x);
		pElement->SetAttribute("y", shape.GetBoundingRect().y);
		pElement->SetAttribute("width", shape.GetBoundingRect().z);
		pElement->SetAttribute("height", shape.GetBoundingRect().w);
		pElement->SetAttribute("inversed", shape.IsInversed());
		m_pRoot->InsertEndChild(pElement);
	}

	bool Flush()
	{
		return m_document.SaveFile(m_filePath.c_str()) == tinyxml2::XML_SUCCESS;
	}

private:
	tinyxml2::XMLDocument m_document;
	tinyxml2::XMLNode* m_pRoot;
	std::string m_filePath;
};

ShapeSceneSerializer::ShapeSceneSerializer(const std::string& filePath)
	: m_pImpl(std::make_unique<ShapeSceneSerializer::Impl>(filePath))
{
}

ShapeSceneSerializer::~ShapeSceneSerializer()
{
}

void ShapeSceneSerializer::SerializeNode(const ShapeNode& shape)
{
	m_pImpl->SerializeNode(shape);
}

bool ShapeSceneSerializer::Flush()
{
	return m_pImpl->Flush();
}
