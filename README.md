# Shapes on wxWidgets.

![picture](Screenshot.png)

[picture]: https://bitbucket.org/chosti34/wxwidgetstest/src/b7873c874c69bc5c5f56786b451a9f358cce455f/Screenshot.png?at=master&fileviewer=file-view-default

## Used libraries (vcpkg, x64-windows-static)
- wxWidgets (GUI)
- GDIPlus / Direct2D (rendering shapes)
- boost (signals, format)
- GLM (utils for math)
- tinyxml2 (save/load scene from file)
